<div id="content">
 <p>
I’ve  <a href="../emacs-eshell/index.html">used and defended Eshell</a> for years.  Sadly, Eshell has some long
standing issues that I grew tired of in the long run.  So I’ve decided to switch
to  <code>M-x shell</code> and see how much of my Eshell workflow I could port.
</p>

 <div id="outline-container-orga49510f" class="outline-2">
 <h2 id="orga49510f">Language and the underlying shell program</h2>
 <div class="outline-text-2" id="text-orga49510f">
 <p>
The benefit of using Bash is that it’s the  <i>de facto</i> standard for sharing shell
commands on the Internet.  As such,  <code>M-x shell</code> can run any of those shell
snippets out there without extra modification.
</p>

 <p>
Eshell constantly requires modifications to the syntax, for instance turning
 <code>$(...)</code> to  <code>${...}</code> or the input redirection  <code><</code> to a pipe.
</p>

 <p>
You might complain that Bash is a terrible language and Eshell’s Elisp is vastly
superior.  But even at the language level,  <code>M-x shell</code> beats Eshell since it
lets the user choose which underlying program to run (with  <code>C-u M-x shell</code>).  So
the same mode can run  <code>psql</code>,  <code>bash</code>, or  <a href="https://github.com/willghatch/racket-rash"> <code>rash</code></a> if you fancy a Racket-powered
shell.  And Racket is a much more interesting language than Eshell in my
opinion :)
</p>
</div>
</div>

 <div id="outline-container-org8746d4b" class="outline-2">
 <h2 id="org8746d4b">Virtualenv and  <code>guix environment</code></h2>
 <div class="outline-text-2" id="text-org8746d4b">
 <p>
 <code>virtualenv</code> and other environment managers don’t work well with Eshell.
Maybe it would be possible to leverage the  <code>direnv</code> Emacs package, but I haven’t
looked into it yet.
</p>

 <p>
I’ve heard that  <code>nix-env</code> has support for Eshell, so I suppose it would be
equally possible to add Eshell support to  <code>guix environment</code>.
</p>

 <p>
Until then,  <code>M-x shell</code> has better support for environment managers than Eshell.
</p>
</div>
</div>

 <div id="outline-container-org666beb7" class="outline-2">
 <h2 id="org666beb7">Completion</h2>
 <div class="outline-text-2" id="text-org666beb7">
 <p>
Eshell only offers poor completion by default.
 <code>M-x shell</code> can do slightly better if the underlying shell is Bash, but the
latter does not always provide great completion.
</p>

 <p>
It’s possible to greatly enhance completion with support from the Fish shell.
The  <code>fish-completion</code> package supports both  <code>M-x shell</code> and Eshell.
</p>

 <p>
You can even display the Fish inline documentation with the
 <code>helm-fish-completion</code> package.
</p>
</div>
</div>

 <div id="outline-container-orgcb88f18" class="outline-2">
 <h2 id="orgcb88f18">Performance</h2>
 <div class="outline-text-2" id="text-orgcb88f18">
 <p>
 <code>M-x shell</code> is significantly faster than Eshell at processing long outputs.
To the point that performance is rarely an issue (never was for me at least).
In Eshell, it is not uncommon to see Emacs grind to a halt because of too long
an output.
</p>

 <p>
 <code>M-x shell</code> is still an order of magnitude slower than, say,  <code>emacs-vterm</code>.  The
performance issue is significantly reduced when  <code>font-lock-mode</code> is off.  There
may be a few other tricks to boost  <code>M-x shell</code> performance.
</p>
</div>
</div>

 <div id="outline-container-org559b76d" class="outline-2">
 <h2 id="org559b76d">Command duration</h2>
 <div class="outline-text-2" id="text-org559b76d">
 <p>
In Eshell, I implemented a special prompt that reports the duration of the previous
command.  This is very useful because I don’t have to think  <i>beforehand</i> whether
I should prefix my command with  <code>time</code>.  And I typically don’t want to run a
slow command twice just to know how much time it took.
</p>

 <p>
This Eshell prompt is available in the  <code>eshell-prompt-extras</code> package:
</p>

 <div class="org-src-container">
 <pre class="src src-elisp">(setq eshell-prompt-function #'epe-theme-multiline-with-status)
</pre>
</div>

 <p>
Sadly, there is no equivalent to  <code>eshell-post-command-hook</code> for  <code>M-x shell</code>
which makes it impossible to implement using the same (sane) logic.
</p>

 <p>
So I had to resort to a hack.  I added the following to my  <code>.bashrc</code>
</p>

 <div class="org-src-container">
 <pre class="src src-sh">PS1='\e[1;37m[\e[1;32m\w\e[1;37m]\e[m \e[0;34m\D{%F %T}\e[m\n\e[1;37m\$\e[m '
</pre>
</div>

 <p>
to get a prompt looking like this:
</p>

 <div class="org-src-container">
 <pre class="src src-sh">[~/dotfiles] 2020-06-26 16:36:30
$ echo Hi!
</pre>
</div>

 <p>
With a bit of Elisp, I was able to write a  <a href="https://gitlab.com/ambrevar/dotfiles/-/blob/adce533dc244f3e4fd5c146172eb48981c73b5b2/.emacs.d/lisp/init-shell.el#L68"> <code>ambrevar/shell-command-duration</code></a>
command that parses the prompt and reports the time a command took.
It’s far from perfect but it’s a start.
</p>
</div>
</div>

 <div id="outline-container-orgf4ecf03" class="outline-2">
 <h2 id="orgf4ecf03">Helm-system-packages</h2>
 <div class="outline-text-2" id="text-orgf4ecf03">
 <p>
As mentioned above,  <code>M-x shell</code> does not have an equivalent to
 <code>eshell-post-command-hook</code> which makes it impossible to use it for
 <code>helm-system-package</code>.
</p>
</div>
</div>

 <div id="outline-container-org2053877" class="outline-2">
 <h2 id="org2053877">Extra syntax highlighting (fontification)</h2>
 <div class="outline-text-2" id="text-org2053877">
 <p>
Comint-mode, the major mode behind  <code>M-x shell</code>, supports extra fontification by
default.  For instance, running
</p>

 <div class="org-src-container">
 <pre class="src src-sh">$ ip addr
</pre>
</div>

 <p>
prints the network interfaces in a different colour.  Neat, isn’t it?
</p>
</div>
</div>

 <div id="outline-container-org6edeff2" class="outline-2">
 <h2 id="org6edeff2">Helm “Switch to shell”</h2>
 <div class="outline-text-2" id="text-org6edeff2">
 <p>
One of the features I use the most is the “Switch to to Eshell” action from
 <code>helm-find-files</code>.  Until recently, it only supported Eshell.
</p>

 <p>
This is now fixed in  <a href="https://github.com/emacs-helm/helm/commit/6061a3af5139491d7811c86d7c9cd5fec8f5fb40">Helm 3.6.3</a> and I just had to add this to my initialization
file:
</p>

 <div class="org-src-container">
 <pre class="src src-elisp">(setq helm-ff-preferred-shell-mode 'shell-mode)
</pre>
</div>

 <p>
I can now quickly fuzzy-search and switch to the directly I want without ever
typing a single  <code>cd</code> command.
</p>
</div>
</div>

 <div id="outline-container-org37f4f9a" class="outline-2">
 <h2 id="org37f4f9a">Narrow-to-prompt</h2>
 <div class="outline-text-2" id="text-org37f4f9a">
 <p>
A very convenient Emacs command is  <code>narrow-to-defun</code> ( <code>C-x n d</code>): it focuses the
buffer on a single function definition and all buffer-global commands are
restricted to it.  For instance, if I want to replace all occurrences of  <code>foo</code>
in a given function, without altering the other  <code>foo</code> in the rest of the buffer,
I can first narrow to the function, then run  <code>query-replace</code> over all visible
occurrences.
</p>

 <p>
I wanted to do the same with shells.  Indeed, it’s very useful to be able to
restrict commands to the output of a given command, say, to search an output
without hitting matches from other outputs in the same buffer.
</p>

 <p>
I wrote an implementation both for  <a href="https://gitlab.com/ambrevar/dotfiles/-/blob/adce533dc244f3e4fd5c146172eb48981c73b5b2/.emacs.d/lisp/init-eshell.el#L312">Eshell</a> and  <a href="https://gitlab.com/ambrevar/dotfiles/-/blob/adce533dc244f3e4fd5c146172eb48981c73b5b2/.emacs.d/lisp/init-shell.el#L84"> <code>M-x shell</code></a>.
</p>
</div>
</div>

 <div id="outline-container-org309e239" class="outline-2">
 <h2 id="org309e239">Browsing prompts</h2>
 <div class="outline-text-2" id="text-org309e239">
 <p>
It’s useful to search prompts, maybe to copy a command or to consult its output
again.
</p>

 <p>
Helm can fuzzy-search and browse the prompts of all shell (including Eshell)
buffers with  <code>helm-comint-prompts-all</code> and  <code>helm-eshell-prompts-all</code>.
</p>
</div>
</div>

 <div id="outline-container-orgeed74a5" class="outline-2">
 <h2 id="orgeed74a5">Global, filtered history</h2>
 <div class="outline-text-2" id="text-orgeed74a5">
 <p>
More often than not, we use multiple shells.  By default, the shell history is
not synchronized between the shells and, worse, it gets overwritten by the  <code>M-x
shell</code> that was last closed, which means that all previously closed shell
histories are gone.
</p>

 <p>
It’s possible to fix this issue with the right shell setup (e.g. see
 <a href="https://unix.stackexchange.com/questions/1288/preserve-bash-history-in-multiple-terminal-windows">this discussion</a> for Bash) but it’s limited and it’s not general enough since it
must be done for all shell sub-programs, when supported at all.
</p>

 <p>
A better approach is to ignore the underlying shell history and use Emacs
capabilities.  Eshell has the same limitations by default which I had fixed with
some custom Elisp, so I just ported it to  <code>comint-mode</code> and voilà!
</p>

 <p>
While I was at it, I’ve also applied history filtering such as removing  <i>all</i>
duplicates (not just when last command matches the last history entry) and other
undesirable commands, such as  <code>cd ...</code> or commands starting with a space.
</p>

 <div class="org-src-container">
 <pre class="src src-elisp">(defun ambrevar/ring-delete-first-item-duplicates (ring)
  "Remove duplicates of last command in history.
Return RING.

Unlike `eshell-hist-ignoredups' or `comint-input-ignoredups', it
does not allow duplicates ever.  Surrounding spaces are ignored
when comparing."
  (let ((first (ring-ref ring 0))
	(index 1))
    (while (<= index (1- (ring-length ring)))
      (if (string= (string-trim first)
		   (string-trim (ring-ref ring index)))
	  ;; We don't stop at the first match so that from an existing history
	  ;; it cleans up existing duplicates beyond the first one.
	  (ring-remove ring index)
	(setq index (1+ index))))
    ring))

(defvar ambrevar/shell-history-global-ring nil
  "The history ring shared across shell sessions.")

(defun ambrevar/shell-use-global-history ()
  "Make shell history shared across different sessions."
  (unless ambrevar/shell-history-global-ring
    (when comint-input-ring-file-name
      (comint-read-input-ring))
    (setq ambrevar/shell-history-global-ring (or comint-input-ring (make-ring comint-input-ring-size))))
  (setq comint-input-ring ambrevar/shell-history-global-ring))

(defun ambrevar/shell-history-remove-duplicates ()
  (require 'functions) ; For `ambrevar/ring-delete-first-item-duplicates'.
  (ambrevar/ring-delete-first-item-duplicates comint-input-ring))

(defvar ambrevar/comint-input-history-ignore (concat "^" (regexp-opt '("#" " " "cd ")))
  "`comint-input-history-ignore' can only be customized globally
because `comint-read-input-ring' uses a temp buffer.")

(defun ambrevar/shell-remove-ignored-inputs-from-ring ()
  "Discard last command from history if it matches
`ambrevar/comint-input-history-ignore'."
  (unless (ring-empty-p comint-input-ring)
    (when (string-match ambrevar/comint-input-history-ignore
			(ring-ref comint-input-ring 0))
      (ring-remove comint-input-ring 0))))

(defun ambrevar/shell-sync-input-ring (_)
  (ambrevar/shell-history-remove-duplicates)
  (ambrevar/shell-remove-ignored-inputs-from-ring)
  (comint-write-input-ring))

(defun ambrevar/shell-setup ()
  (setq comint-input-ring-file-name
	(expand-file-name "shell-history" user-emacs-directory))
  (ambrevar/shell-use-global-history)

  ;; Write history on every command, not just on exit.
  (add-hook 'comint-input-filter-functions 'ambrevar/shell-sync-input-ring nil t))

(add-hook 'shell-mode-hook 'ambrevar/shell-setup)
</pre>
</div>
</div>
</div>

 <div id="outline-container-orgbf1ba91" class="outline-2">
 <h2 id="orgbf1ba91">Emacs integration</h2>
 <div class="outline-text-2" id="text-orgbf1ba91">
 <p>
One of the benefits of Eshell is that it integrates shell commands with Emacs.
For instance, running  <code>grep</code> will display an interactive result in an Emacs
buffer.
</p>

 <p>
It’s possible to write an  <code>emacsclient</code> wrapper that evaluates the command
passed as argument in an Emacs buffer to, so it’s possible to mimic this feature
of Eshell rather closely.
</p>

 <p>
Still, this is not as closely integrated to Emacs as it could get.  Indeed,
Eshell can intertwine its shell language with Elisp.  It’s thus able to run any
Elisp function.
</p>

 <p>
Maybe a good direction to explore is  <a href="http://howardism.org/Technical/Emacs/piper-presentation-transcript.html">piper</a>.
</p>
</div>
</div>

 <div id="outline-container-orgd84c89b" class="outline-2">
 <h2 id="orgd84c89b">Conclusion</h2>
 <div class="outline-text-2" id="text-orgd84c89b">
 <p>
I’m happy with  <code>M-x shell</code>, the everyday use is much smoother than that of
Eshell.  Performance being one of the biggest selling point in my experience.
</p>

 <p>
Overall, with the support of few packages such as Helm and  <code>helm-fish-completion</code>
I get a stellar shell experience.  I miss very few features, such as support for
“visual” commands, modifiers and predicates which I rarely use.
</p>
</div>
</div>
</div>