<div id="wpbody">JavaScript is a programming language that gives our web applications and web pages the ability to think and act by making it interactive and dynamic. Like any other programming language, JavaScript offers us functions which are a set of defined commands or statements that are executed only when we call the function that has this code. The function takes an input or some arguments and returns the output. The input arguments can be passed by value or reference. </p>
<p>So, in this post, we&#8217;ll define the terms &#8220;pass by value&#8221; and &#8220;pass by reference&#8221; along with examples in JavaScript, as well as explain the differences between the two.</p>
<h2>What is pass-by-value?</h2>
<p>A function is called directly by sending the value of the variable as an argument if that function is pass-by-value. As a result, any changes made within the function have no impact on the initial or original value. The original value isn’t changed because when we pass the variable into a function as an argument, the copy of that variable is created and hence any changes or operations performed inside that function are done on the copy variable rather than the original one.</p>
<h2>Pass by value Example</h2>
<p>Let us create a function with the name of <strong>passByValue </strong>and change the values of the variables <strong>a</strong> and <strong>b</strong> that are passed as arguments in this function. Outside the function, we initialize the a and b variables and give them 1 and 2 values respectively. Then we console log these values.</p>
<div class="codecolorer-container javascript blackboard" style="border:1px solid #9F9F9F;width:435px;height:100%;"><div class="javascript codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;"><span style="color: #000066; font-weight: bold;">function</span> passByValue<span style="color: #009900;">&#40;</span>a<span style="color: #339933;">,</span> b<span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#123;</span><br />
<br />
&nbsp; &nbsp; a<span style="color: #339933;">=</span><span style="color: #CC0000;">3</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; b<span style="color: #339933;">=</span><span style="color: #CC0000;">4</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span><span style="color: #3366CC;">&quot;Inside the function&quot;</span><span style="color: #009900;">&#41;</span><br />
<br />
&nbsp; &nbsp; console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span><span style="color: #3366CC;">&quot;a: &quot;</span><span style="color: #339933;">,</span>a<span style="color: #339933;">,</span> <span style="color: #3366CC;">&quot; b: &quot;</span><span style="color: #339933;">,</span>b<span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> <span style="color: #006600; font-style: italic;">// 3, 4</span><br />
<br />
<span style="color: #009900;">&#125;</span><br />
<br />
&nbsp;<br />
let a <span style="color: #339933;">=</span> <span style="color: #CC0000;">1</span><span style="color: #339933;">;</span><br />
<br />
let b <span style="color: #339933;">=</span> <span style="color: #CC0000;">2</span><span style="color: #339933;">;</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span><span style="color: #3366CC;">&quot;Outside Function. Before calling function&quot;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span><span style="color: #3366CC;">&quot;a: &quot;</span><span style="color: #339933;">,</span>a<span style="color: #339933;">,</span> <span style="color: #3366CC;">&quot; b: &quot;</span><span style="color: #339933;">,</span>b<span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> <span style="color: #006600; font-style: italic;">// 1,2</span><br />
<br />
&nbsp;<br />
passByValue<span style="color: #009900;">&#40;</span>a<span style="color: #339933;">,</span> b<span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp;<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span><span style="color: #3366CC;">&quot;Outside Function. After calling function&quot;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span><span style="color: #3366CC;">&quot;a: &quot;</span><span style="color: #339933;">,</span>a<span style="color: #339933;">,</span> <span style="color: #3366CC;">&quot; b: &quot;</span><span style="color: #339933;">,</span>b<span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> <span style="color: #006600; font-style: italic;">// 1,2</span></div></div>
<p>We will see that when we console log the values of a and b outside the function it will say 1 and 2. However, inside the function, the values will be 3 and 4 and again after calling this function the values won’t change as inside the function copies were made of a and b and changes were made to those copies.</p>
<p><img src="https://linuxhint.com/wp-content/uploads/2021/11/1-16.jpg" alt="" width="460" height="210" class="aligncenter size-full wp-image-131572" srcset="https://linuxhint.com/wp-content/uploads/2021/11/1-16.jpg 460w, https://linuxhint.com/wp-content/uploads/2021/11/1-16-300x137.jpg 300w" sizes="(max-width: 460px) 100vw, 460px" /></p>
<h2>What is pass-by-reference?</h2>
<p>A function is called by supplying the variable&#8217;s reference/address as a parameter in <strong>pass-by reference</strong>. As a result, modifying the value within the function also modifies the value outside the function that is the original value. The pass-by-reference feature is used in JavaScript arrays and objects.</p>
<h2>Pass by Reference Example</h2>
<p>Let us initialize an object and give two properties to it. One property defines the name of the machine and the other “<strong>isOn</strong>” which lets us know whether the machine is on or not. We also initialize a function with the name of <strong>passByReference </strong>and change the value of the computer object properties like name and isOn. We then console log these properties before and after calling the function:</p>
<div class="codecolorer-container javascript blackboard" style="border:1px solid #9F9F9F;width:435px;height:100%;"><div class="javascript codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;"><span style="color: #000066; font-weight: bold;">function</span> passByReference<span style="color: #009900;">&#40;</span>machine<span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#123;</span><br />
<br />
&nbsp; &nbsp; machine.<span style="color: #660066;">name</span><span style="color: #339933;">=</span><span style="color: #3366CC;">&quot;Computer&quot;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; machine.<span style="color: #660066;">isOn</span> <span style="color: #339933;">=</span> <span style="color: #003366; font-weight: bold;">true</span><span style="color: #339933;">;</span><br />
<br />
<span style="color: #009900;">&#125;</span><br />
<br />
<br />
<span style="color: #000066; font-weight: bold;">var</span> computer <span style="color: #339933;">=</span> <span style="color: #009900;">&#123;</span><br />
<br />
&nbsp; &nbsp; name<span style="color: #339933;">:</span> <span style="color: #3366CC;">&quot;myComputer&quot;</span><span style="color: #339933;">,</span><br />
<br />
&nbsp; &nbsp; isOn<span style="color: #339933;">:</span> <span style="color: #003366; font-weight: bold;">false</span><br />
<br />
<span style="color: #009900;">&#125;</span><span style="color: #339933;">;</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span><span style="color: #3366CC;">&quot;Before calling function&quot;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>computer.<span style="color: #660066;">isOn</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> <span style="color: #006600; font-style: italic;">// true;</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>computer.<span style="color: #660066;">name</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> <span style="color: #006600; font-style: italic;">// Computer</span><br />
<br />
<br />
passByReference<span style="color: #009900;">&#40;</span>computer<span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span><span style="color: #3366CC;">&quot;After calling function&quot;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>computer.<span style="color: #660066;">isOn</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> <span style="color: #006600; font-style: italic;">// true;</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>computer.<span style="color: #660066;">name</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> <span style="color: #006600; font-style: italic;">// Computer</span></div></div>
<p><img src="https://linuxhint.com/wp-content/uploads/2021/11/2-16.jpg" alt="" width="288" height="215" class="aligncenter size-full wp-image-131573" /></p>
<p>We can see that copies weren’t made in the function and the original properties of the computer object were changed, hence it is passed by reference.</p>
<h2>Difference Between pass by value and pass by reference</h2>
<p>The major difference between pass by value and pass by reference is that pass by reference comes into play when we assign primitives and pass by value comes into play when we assign objects. Primitive data types include string numbers, boolean, symbols, and values like null and undefined, and the object data types include functions, arrays, and simple objects. </p>
<p>The second major difference between the two is that pass-by-value creates a copy and then changes are made to that copy; however in pass-by-reference no copy is made and modification is done on the original variable.</p>
<h2>Conclusion</h2>
<p>We can pass values into a function via pass by value or pass by reference. Pass by value is done on the primitive data types like string, number, boolean, and every time you pass a variable to a function, it creates a copy of that variable and then modifies that copy in a pass by value. Pass by reference is done on the object data type like functions, arrays, and plain objects, and in the pass by reference, the original value is modified as pass by reference doesn’t create a copy. </p>
<p>In this post, first, we saw what pass by value is and pass by reference is and explained both the phenomena with the help of an example and then continued our discussion by answering the question of what is the difference between pass by value and pass by reference in JavaScript.
 </p></div>
