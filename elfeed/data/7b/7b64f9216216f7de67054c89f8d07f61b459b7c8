<p>Via Paul Graham, he have this:</p>
<blockquote class="twitter-tweet">
<p lang="en" dir="ltr">This looks like a permanent shift, and a really big one too. From 30% to almost 80% just like that. A change on this scale is bound to have all sorts of interesting consequences. <a href="https://t.co/7YfVQj6pSQ">https://t.co/7YfVQj6pSQ</a></p>
<p>— Paul Graham (@paulg) <a href="https://twitter.com/paulg/status/1452359056040022027?ref_src=twsrc%5Etfw">October 24, 2021</a></p></blockquote>
<p><script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></p>
<p>I’ve been writing a lot lately about remote work and how smart management will eventually come to terms with the new reality. The above graph appears to show that it’s already happening. An increase of 50% in job listings offering remote work is significant.</p>
<p>Of course, we’re looking at only one source of listings and that source is Hacker News so we shouldn’t rush into drawing wide ranging conclusions. I think it likely that traditional Wall St. firms, for example, will be slower to embrace remote work as a permanent way of doing business but if they don’t eventually get on board, hungry startups will start eating their lunch.</p>
<p>It’s probably true that Tech jobs in particular are going to have to offer remote work as an option. Heretofore, Apple and Google, for example, have been considered such great gigs that they could insist that everyone come into the office. More and more, that seems like a rearguard action that will soon change.</p>
