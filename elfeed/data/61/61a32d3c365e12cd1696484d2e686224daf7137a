<p>
Update 2021-10-25: color-theme-tangotango removed, as it was mistakenly added.
</p>

<ul class="org-ul">
<li>Upcoming events:
<ul class="org-ul">
<li>Emacs Berlin (virtual, in English) <a href="https://emacs-berlin.org/">https://emacs-berlin.org/</a> Wed Oct 27 0930 Vancouver / 1130 Chicago / 1230 Toronto / 1630 GMT / 1830 Berlin / 2200 Kolkata &#x2013; Thu Oct 28 0030 Singapore</li>
<li>EmacsNYC: Monthly Online Meetup - Lightning Talks <a href="https://www.meetup.com/New-York-Emacs-Meetup/events/281224309/">https://www.meetup.com/New-York-Emacs-Meetup/events/281224309/</a> Mon Nov 1 1600 Vancouver / 1800 Chicago / 1900 Toronto / 2300 GMT &#x2013; Tue Nov 2 0000 Berlin / 0430 Kolkata / 0700 Singapore</li>
<li>EmacsATX: TBD <a href="https://www.meetup.com/EmacsATX/events/281302253/">https://www.meetup.com/EmacsATX/events/281302253/</a> Wed Nov 3 1630 Vancouver / 1830 Chicago / 1930 Toronto / 2330 GMT &#x2013; Thu Nov 4 0030 Berlin / 0500 Kolkata / 0730 Singapore</li>
<li>Emacs Paris (virtual, in French) <a href="https://www.emacs-doctor.com/emacs-paris-user-group/">https://www.emacs-doctor.com/emacs-paris-user-group/</a> Thu Nov 4 0930 Vancouver / 1130 Chicago / 1230 Toronto / 1630 GMT / 1730 Berlin / 2200 Kolkata &#x2013; Fri Nov 5 0030 Singapore</li>
<li>M-x Research (contact them for password): TBA <a href="https://m-x-research.github.io/">https://m-x-research.github.io/</a> Fri Nov 5 0800 Vancouver / 1000 Chicago / 1100 Toronto / 1500 GMT / 1600 Berlin / 2030 Kolkata / 2300 Singapore</li>
</ul></li>
<li>Emacs configuration:
<ul class="org-ul">
<li><a href="https://www.rousette.org.uk/archives/emacs-from-scratch-again/">But She's a Girl: Emacs from scratch again</a> (<a href="https://www.reddit.com/r/planetemacs/comments/qet528/but_shes_a_girl_emacs_from_scratch_again/">Reddit</a>)</li>
<li><a href="https://github.com/shaneikennedy/emacs-light">emacs-light: shaneikennedy's lightweight bare necessities emacs config</a></li>
<li><a href="https://github.com/skyler544/rune-emacs-config">skyler544's emacs-config</a> - inspired by Emacs from Scratch and Doom Emacs</li>
<li><a href="https://github.com/Eason0210/dot-emacs">Eason0210's emacs config</a> - Haskell, C/C++, Python, CSS/LESS/SASS/SCSS, JS/Typescript, HTML, Rust, org-roam</li>
<li><a href="https://github.com/bodnarlajos/emacs-pure">emacs-pure: emacs with native + libjson</a></li>
<li><a href="https://github.com/onetom/onetomacs">onetomacs: onetom's Emacs configuration</a></li>
</ul></li>
<li>Emacs Lisp:
<ul class="org-ul">
<li><a href="https://coredumped.dev/2021/10/21/building-an-emacs-lisp-vm-in-rust/">Building an Emacs lisp virtual machine in Rust</a> (<a href="https://www.reddit.com/r/emacs/comments/qcus3f/building_an_emacs_lisp_virtual_machine_in_rust/">Reddit</a>)</li>
</ul></li>
<li>Appearance:
<ul class="org-ul">
<li><a href="https://i.redd.it/7devi4jy4uu71.png">modus-themes-syntax customization</a> (<a href="https://www.reddit.com/r/emacs/comments/qcwg80/modusthemessyntax_customization/">Reddit</a>)</li>
<li><a href="https://i.redd.it/pakq9p4b4pu71.png">Just made my tab-bar-mode setup a little prettier with unicode icons</a> (<a href="https://www.reddit.com/r/emacs/comments/qcetl3/just_made_my_tabbarmode_setup_a_little_prettier/">Reddit</a>)</li>
<li><a href="https://github.com/mclear-tools/bespoke-modeline">bespoke-modeline: A custom minimalist modeline</a></li>
<li><a href="https://github.com/rougier/nano-bell">nano-bell: Visual bell for GNU Emacs</a></li>
</ul></li>
<li>Navigation:
<ul class="org-ul">
<li><a href="https://karthinks.com/software/avy-can-do-anything/">Karthik Chikmagalur: Avy can do anything</a></li>
</ul></li>
<li>Org Mode:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/qdze6g/new_orgbars_add_bars_to_the_virtual_indentation/">[NEW] org-bars -&gt; add bars to the virtual indentation provided by the built-in package org-indent.</a></li>
<li><a href="https://www.youtube.com/watch?v=QpreUiyJ41Q">Emacs Screencast #5: Mein Org Mode Workflow + Anpassungen</a> (53:26)</li>
<li><a href="https://youssefbouzekri.vercel.app/posts/blogging-setup-hugo-and-org/">How to make a blog with hugo and org-mode using ox-hugo</a> (<a href="https://www.reddit.com/r/orgmode/comments/qca5y6/how_to_make_a_blog_with_hugo_and_orgmode_using/">Reddit</a>)</li>
<li><a href="https://www.youtube.com/watch?v=NimOvf8Fawg">Custom emacs con org #5 org-src and org</a> (14:13)</li>
<li><a href="https://www.youtube.com/watch?v=KE-GjOb58x8">getting covid data from John Hopkins University with python, covid19pandas  and emacs</a> (10:58)</li>
<li><a href="https://www.youtube.com/watch?v=mCFMO60Ywj8">New video: org-ref - Exporting nicely formatted cross-references to non-LaTeX from #orgmode</a> (<a href="https://www.reddit.com/r/orgmode/comments/qc57cs/new_video_orgref_exporting_nicely_formatted/">Reddit</a>)</li>
<li><a href="https://www.youtube.com/watch?v=3u6eTSzHT6s">New video: org-ref version-3 overview</a> (<a href="https://www.reddit.com/r/orgmode/comments/qbl0n1/new_video_orgref_version3_overview/">Reddit</a>)</li>
</ul></li>
<li>Completion:
<ul class="org-ul">
<li><a href="https://protesilaos.com/codelog/2021-10-22-emacs-mct-demo/">Protesilaos Stavrou: Demo of my Minibuffer and Completions in Tandem (mct.el) for Emacs</a> (<a href="https://www.youtube.com/watch?v=roSD50L2z-A">24:38</a>, <a href="https://www.reddit.com/r/emacs/comments/qdrt71/prot_minibuffer_and_completions_in_tandem/">Reddit</a>)</li>
</ul></li>
<li>Coding:
<ul class="org-ul">
<li><a href="https://github.com/jsalzbergedu/pseudocode-mode">pseudocode-mode: A major and minor mode for pseudocode syntax highlighting</a></li>
<li><a href="https://i.redd.it/99k5adjddfu71.png">How about my flutter dev Emacs setup</a> (<a href="https://www.reddit.com/r/emacs/comments/qbduik/how_about_my_flutter_dev_emacs_setup/">Reddit</a>)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/qca1re/til_emacs_has_a_hex_editor/">TIL Emacs has a hex editor</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/qefx8e/til_idl_is_a_thing_and_emacs_has_it/">TIL IDL is a Thing and Emacs Has It</a> - Interactive Data Language, IDLWAVE</li>
<li><a href="https://www.draketo.de/software/emacs-javascript.html">Using Emacs for Javascript development</a> (2020, <a href="https://www.reddit.com/r/planetemacs/comments/qblms1/using_emacs_for_javascript_development/">Reddit</a>)</li>
<li><a href="https://tech.toryanderson.com/2021/10/18/html-project-in-emacs/">Tory Anderson: HTML project in emacs</a></li>
<li><a href="https://github.com/emacs-eaf/eaf-jupyter">eaf-jupyter: Jupyter in Emacs</a></li>
<li>Version control:
<ul class="org-ul">
<li><a href="https://emacsair.me/2021/10/21/git-modes">Jonas Bernoulli: Git-Commit available from NonGNU Elpa and Melpa</a> (<a href="https://www.reddit.com/r/emacs/comments/qcec4k/gitmodes_now_available_from_nongnu_elpa_and_melpa/">Reddit</a>)</li>
<li><a href="https://github.com/Artawower/blamer.el">Blamer.el: a git blame package inspired by VS Code's GitLens plugin</a> (<a href="https://www.reddit.com/r/emacs/comments/qbmsp8/blamerel_another_one_blame_package/">Reddit</a>)</li>
<li><a href="https://emacsair.me/2017/09/01/the-magical-git-interface/">Magit, the magical Git interface</a> (2017, <a href="https://news.ycombinator.com/item?id=28954058">HN</a>, <a href="https://www.reddit.com/r/emacs/comments/qdqdxm/magit_hits_hacker_news_frontpage_again/">Reddit</a>)</li>
</ul></li>
</ul></li>
<li>EXWM:
<ul class="org-ul">
<li><a href="https://www.youtube.com/watch?v=O3nOnrc2WIs">EXWM - Emacs X Window Manager On GNU/Emacs With  Dmenu</a> (10:35)</li>
<li><a href="https://lists.gnu.org/archive/html/emacs-devel/2021-10/msg01377.html">EXWM new maintainer</a> (<a href="https://www.reddit.com/r/emacs/comments/qb13jk/exwm_new_maintainer/">Reddit</a>)</li>
</ul></li>
<li>Community:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/qbvyza/weekly_tips_tricks_c_thread/">Weekly Tips, Tricks, &amp;c. Thread</a></li>
<li><a href="https://protesilaos.com/codelog/2021-10-21-emacs-conf-presentation/">Protesilaos Stavrou: My talk at EmacsConf 2021 and its backstory</a></li>
</ul></li>
<li>Other:
<ul class="org-ul">
<li><a href="https://www.youtube.com/watch?v=xDgpXbjeMeo">Emacs Keyboard Macro: Make Numbered Lists Quickly</a> (03:57)</li>
<li><a href="https://www.youtube.com/watch?v=SCZZnVkXOAg">Custom emacs con org #6 undo-tree and hydra</a> (14:05)</li>
<li><a href="https://github.com/duckwork/filldent.el">[ANN] filldent.el: Fill or indent, depending on mode</a></li>
<li><a href="https://ag91.github.io/blog/2021/10/22/moldable-emacs-how-to-get-useful-info-about-a-buffer-without-reading-it/">Moldable Emacs: how to get useful info about a buffer without reading it</a> (<a href="https://www.reddit.com/r/emacs/comments/qdpkcj/moldable_emacs_how_to_get_useful_info_about_a/">Reddit</a>)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/qehd3c/build_emacs_with_native_compilation_in_macos/">Build emacs with native compilation in macOS</a></li>
<li><a href="https://tech.toryanderson.com/2021/10/18/how-to-check-keybindings-in-isearch-mode/">Tory Anderson: How to check keybindings in isearch mode</a></li>
<li><a href="http://ag91.github.io/blog/2021/10/22/moldable-emacs-how-to-get-useful-info-about-a-buffer-without-reading-it">Andrea: Moldable Emacs: how to get useful info about a buffer without reading it</a></li>
<li><a href="https://github.com/vsalvino/emacs">GitHub - vsalvino/emacs: Emacs for Windows 10/11 with Dark Mode</a> (<a href="https://news.ycombinator.com/item?id=28975218">HN</a>, <a href="https://lists.gnu.org/archive/html/emacs-devel/2021-10/msg01737.html">emacs-devel</a>)</li>
</ul></li>
<li>Emacs development:
<ul class="org-ul">
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=79f7e87da5037f22be07954bb8000ee88e18e515">image-dired: Add support for GraphicsMagick</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=057f45abeced1c81bd9bb2782ab6a8fe472f38c8">Support new Thumbnail Managing Standard sizes in image-dired</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=c2055d41b4b145aa940ce940adc1a3fabfe87a6b">Add new macro `with-delayed-message'</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=68d11cc248524750c008a418faca4a9182d51758">Add new option help-link-key-to-documentation</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=860e8c524bc1695b67fe1001412529da47b11138">Make dired directory components clickable</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=ad7fd3cb47fbc8406504a13e8e4d64f650b514ce">Partially remove exiftool dependency from image-dired.el</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=bb06d5648eac297e5574b15c7723903c8c922d03">Add new function exif-field</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=bc2a5c112796ea9f072984b471f980e4321263b3">Add WebP image format support (Bug#51296)</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=65fd3ca84f75aee0dfebb87fa793dae57c1caf35">Support the "medium" font weight</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=442101433c6459202e325264e3bbf97790f512e6">Add new macro with-locale-environment</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=90266b8356dc5e7e6e437fb37b49970205b01408">Tweak how 'align' and 'align-regexp' align text</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=53bea8796d52d90d09c29780070442d59e1883b7">Make downcasing unibyte strings in Turkish less wrong</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=0be85f22176730b951e9ec18e298c9e1e039d8fb">SQL mode supports sending passwords in process</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=84d8df59703b9cb708f6ce044a5d36690425e392">Mention that we now install the pdmp file with a fingerprinted name</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=32df2034234056bf24312ef5883671b59a387520">Remove the "def" indentation heuristic</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=9f505c476eb1a8e85ba26964abf218cab7db0e57">New option show-paren-context-when-offscreen</a></li>
</ul></li>
<li>New packages:
<ul class="org-ul">
<li><a target="_blank" href="https://elpa.gnu.org/packages/sketch-mode.html">sketch-mode</a>: Quickly create svg sketches using keyboard and mouse</li>
<li><a target="_blank" href="http://melpa.org/#/git-modes">git-modes</a>: Major modes for editing Git configuration files</li>
<li><a target="_blank" href="http://melpa.org/#/consult-projectile">consult-projectile</a>: Consult integration for porjectile</li>
<li><a target="_blank" href="http://melpa.org/#/eldoc-toml">eldoc-toml</a>: TOML table name at point for ElDoc</li>
<li><a target="_blank" href="http://melpa.org/#/org-re-reveal-citeproc">org-re-reveal-citeproc</a>: Citations and bibliography for org-re-reveal</li>
<li><a target="_blank" href="http://melpa.org/#/prettify-math">prettify-math</a>: Prettify math formula</li>
<li><a target="_blank" href="http://melpa.org/#/project-tab-groups">project-tab-groups</a>: Support a "one tab group per project" workflow</li>
<li><a target="_blank" href="http://melpa.org/#/shelldon">shelldon</a>: An enhanced shell interface</li>
</ul></li>
</ul>

<p>
Links from <a href="https://www.reddit.com/r/emacs">reddit.com/r/emacs</a>, <a href="https://www.reddit.com/r/orgmode">r/orgmode</a>, <a href="https://www.reddit.com/r/spacemacs">r/spacemacs</a>, <a href="https://www.reddit.com/r/planetemacs">r/planetemacs</a>, <a href="https://hn.algolia.com/?query=emacs&amp;sort=byDate&amp;prefix&amp;page=0&amp;dateRange=all&amp;type=story">Hacker News</a>, <a href="https://planet.emacslife.com">planet.emacslife.com</a>, <a href="https://www.youtube.com/playlist?list=PL4th0AZixyREOtvxDpdxC9oMuX7Ar7Sdt">YouTube</a>, <a href="http://git.savannah.gnu.org/cgit/emacs.git/log/etc/NEWS">the Emacs NEWS file</a>, <a href="https://emacslife.com/calendar/">Emacs Calendar</a> and <a href="http://lists.gnu.org/archive/html/emacs-devel/2021-10">emacs-devel</a>.
</p>
