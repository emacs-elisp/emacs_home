<div id="content">
 <p>
The following article deals with techniques to optimize comfort and speed using
a computer.
</p>

 <div id="outline-container-org95f767c" class="outline-2">
 <h2 id="org95f767c">Touch typing</h2>
 <div class="outline-text-2" id="text-org95f767c">
 <p>
Writing text holds a central part in our use of computers: e-mails, searching,
web browsing, programming, configuring, etc.
</p>

 <p>
Touch typing is the art of typing (fast) without having to look at the keyboard.
This is a skill that is surprisingly left aside, even by professionals.  I
believe it to be a big time saver, while alleviating the frustration of too much
stuttering and too many typos.
</p>

 <p>
Touch typing can be trained in a fairly short amount of time.  One way would be
to use a trainer program such as  <a href="https://www.gnu.org/software/gtypist/">GNU Typist</a>.  It is straightforward to go
through the various lessons and the result will be immediately noticeable.
</p>

 <p>
Touch typing is highly dependent on the keyboard layout (a.k.a.  <i>keymap</i>), so
you might want to choose the keymap wisely before starting the training.  Read on
for tips on keymaps.
</p>
</div>
</div>

 <div id="outline-container-org224ebe2" class="outline-2">
 <h2 id="org224ebe2">Mouse-less control</h2>
 <div class="outline-text-2" id="text-org224ebe2">
 <p>
The mouse use has increased tremendously since the rise of graphical user
interfaces.  Mostly for illegitimate reasons.  Do we really need a mouse to select
objects or to toggle buttons?
</p>

 <p>
The mouse proves to be poor at selecting text.  How many times have you tried to
select a sentence and missed the last letter?  It is equally bad at selecting
objects.  When it comes to interact with the user interface, it is usually faster
to use keyboard shortcuts.  Well, we call them “shortcuts” for a reason…
</p>

 <p>
The use of a mouse makes sense when there is a need for a continuous input, such
as in graphics design, video games, etc.
</p>
</div>
</div>

 <div id="outline-container-org69935ea" class="outline-2">
 <h2 id="org69935ea">Home row vs. arrows</h2>
 <div class="outline-text-2" id="text-org69935ea">
 <p>
The  <i>home row</i> refers to the center row of the alphabetical part of the
keyboard, that is, the characters  <code>asdf…jkl;</code> on a QWERTY keyboard.  The
standard position is when the index fingers rest on the characters  <code>f</code> and  <code>j</code>
on a QWERTY keyboard.  Those letters usually come with a bump to make them
distinguishable without looking.
</p>

 <p>
Moving hands from the home row to the arrows back and forth can be a small waste
of time that quickly stacks up.  The time required for the “context switch” of
the hand disturbs the flow.
</p>

 <p>
Arrows tend to be omnipresent when it comes to “regular” text editing or
interface navigation.  Now there are various changes we can make to the
environment so that the navigation bindings stick around the home row.
</p>

 <p>
First of, you may consider switching your text editor for one that allows
navigation without arrows.  Famous examples include  <i>Emacs</i> and  <i>Vim</i>.
</p>

 <p>
The window manager can have limited bindings to such an extent that it forces
the use of arrows or the mouse.  Decent window managers usually feature full
keyboard control.  Popular examples include  <i>Awesome</i> and  <i>i3</i>.
</p>

 <p>
Web browsers have become more and more dominant in our use of computers.  The way
the World Wide Web was designed has put emphasis on the mouse, so that it is now
almost impossible to browse the web without a mouse.  Which might be a sign for
poor design from the ground up.  But let’s not drift off too much.  It is still
possible to use a graphical web browser while making best use of the keyboard
thanks to the “hint” feature.  Many Webkit-based browsers offer this feature.  It
is also possible to edit any field using your favourite editor, which greatly
alleviates the need for a mouse and arrows.
</p>

 <p>
If you have got the chance to witness a hardcore geek with proper touch typing
skills and a keyboard / home row centered environment, you will be amazed by how
many actions per minute that geek can perform!
</p>
</div>
</div>

 <div id="outline-container-orgb9ec920" class="outline-2">
 <h2 id="orgb9ec920">Caps-Lock switch</h2>
 <div class="outline-text-2" id="text-orgb9ec920">
 <p>
The Caps-Lock key is very accessible albeit little used.  On the other hand, the
use frequency of keys such as  <code>Control</code> or  <code>Escape</code> is much higher (in
particular when using the Emacs or Vim text editors).
</p>

 <p>
Therefore it is very recommended to swap Caps-Lock with the key you use most.
There are several ways of doing this, read on for an example.
</p>

 <p>
This is one of the keymap tweaks that will save you most from wrecking your
hands with some carpal tunnel syndrome.
</p>
</div>
</div>

 <div id="outline-container-org0d50536" class="outline-2">
 <h2 id="org0d50536">International custom keymaps</h2>
 <div class="outline-text-2" id="text-org0d50536">
 <p>
Users of languages using a Latin-based alphabet should be familiar with the
existence of a variety of “standard” keymaps out there: QWERTY (US, UK…),
QWERTZ, AZERTY, to name a few.
</p>

 <p>
If you find yourself writing in more than one language, you will often find the
need to switch keymap so that you can write some special characters easily.  This
is a big mistake, as the context switch between the various layouts can be
extremely disturbing and require minutes if not hours each time before feeling
comfortable again.
</p>

 <p>
Letters and punctuation often vary between keymaps.  (AZERTY and QWERTY are good
examples of this.)  While additional special characters are welcome, the
positional alteration of standard characters is not strictly necessary.  So what
if we would have a keymap that contains special characters for various languages
at the same time?  There is no such standard keymap, but it is possible to create
one yourself.
</p>

 <p>
Using one single custom keymap has the advantage of eliminating the context
switch disturbance while providing direct access to all the desired special
characters.  Besides, it is possible to base the new keymap on QWERTY US, which
has some inherent benefits:
</p>

 <ul class="org-ul"> <li>Matching parentheses are next to each other.  Punctuation tends to be
reasonably accessible (e.g.  <code>,</code> and  <code>.</code> are unshifted).</li>

 <li>It is the most widespread keymap, so when somebody wants to use your computer,
chances are high they can type something.</li>

 <li>Most importantly, some programs are ergonomically optimized for QWERTY US,
such as Emacs and Vim.</li>
</ul> <p>
Bonus for scientists: it is possible to add some common mathematical characters,
such as  <code>≠</code> or  <code>⋅</code>, which can be a big time saver when it comes to writing
scientific documents.
</p>
</div>

 <div id="outline-container-org849ebd7" class="outline-3">
 <h3 id="org849ebd7">Custom Xkb keymaps</h3>
 <div class="outline-text-3" id="text-org849ebd7">
 <p>
Let’s move on to the details on how to load a custom layout for the X window
system without needing administrative rights.
</p>

 <p>
In the following, we will use the  <code>xkb</code> folder as our workspace.  This folder is
arbitrary.  Replace the keymap name  <code>custom</code> with any unused name you like.
</p>

 <p>
Create an  <code>xkb/custom.xkb</code> file:
</p>

 <pre class="example" id="org210ca1e">
xkb_keymap {
    xkb_keycodes  { include "evdev+aliases(qwerty)" };
    xkb_types     { include "complete" };
    xkb_compat    { include "complete" };
    xkb_symbols   { include "pc+custom+inet(evdev)" };

    // Geometry is completely optional.
    // xkb_geometry  { include "pc(pc104)"  };
};
</pre>

 <p>
Fill in  <code>xkb/symbols/custom</code>.  This file is formatted just like every other Xkb
symbol files, usually located in  <code>X11/xkb/symbols/</code> under  <code>/usr/share</code> or
 <code>/usr/local/share</code>.
</p>

 <p>
Finally, load the new keymap with
</p>

 <div class="org-src-container">
 <pre class="src src-sh">$ xkbcomp -I"xkb" "xkb/custom.xkb" $DISPLAY
</pre>
</div>

 <p>
For a concrete example, see  <a href="https://gitlab.com/ambrevar/dotfiles/tree/master/.xkb/">my personal keymap</a>.  It is a superset of QWERTY US
which covers almost every language in western Europe, with Caps-Lock and Control
swapped.
</p>
</div>
</div>
</div>
</div>