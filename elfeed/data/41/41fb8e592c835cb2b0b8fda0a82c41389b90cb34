
            <p>I am running Emacs 27.1 on Ubuntu.</p>
<p>I have installed <code>gif-screencast</code> and have gotten it to start recording whenever i start Emacs by adding</p>
<pre><code>(gif-screencast)
</code></pre>
<p>to my <code>init.el</code>. So far, so good. The problem is saving the recording when killing Emacs. I have tried to add both</p>
<pre><code>(add-hook 'kill-emacs-hook 'gif-screencast-stop)
</code></pre>
<p>and</p>
<pre><code>(setq confirm-kill-emacs 'gif-screencast-stop)
</code></pre>
<p>to my <code>init.el</code>, but none of them manage to stop the recording and save it <em>before</em> Emacs asks me whether I want to kill running processes. When I answer 'yes', it will, of course, kill the <code>gif-screencast</code> process before ending it properly.</p>
<p>My current solution, arrived at by trial and error, is to add</p>
<pre><code>(advice-add 'save-buffers-kill-emacs :before #'gif-screencast-stop)
</code></pre>
<p>to <code>init.el</code>. This works when killing Emacs by using the GUI menu, but not when using <code>C-x C-c</code> from within Emacs. When I try the latter, i get <code>Wrong number of arguments: (0.0), 1</code>. When I do the former, it correctly saves the <code>gif-screencast</code> recording before giving me the <code>Save file ...?</code> prompt and then killing Emacs.</p>
<p>Edit: it gives me 2 prompts:
First, it gives me <code>Active processes exist; kill them and exit anyway?</code>, then it gives me <code>Save file ...?</code>. The first one of these is the one that has to wait until after <code>gif-screencast</code> has properly finished.</p>
<p>My syntax is wrong somehow, but I'm not experienced enough with <code>elisp</code> to figure it out.</p>
<p>How do I make this execute correctly also when killing Emacs with <code>C-x C-c</code>?</p>

        