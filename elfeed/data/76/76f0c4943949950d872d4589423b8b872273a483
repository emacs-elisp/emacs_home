
<p>&nbsp; La preocupación por la posible transmisión del virus a través del humo de cigarrillo exhalado por fumadores es perfectamente legítima, pero no está justificada ni basada en ninguna evidencia. De hecho, sólo hay un estudio de un grupo de pakistaníes  (ver <a href="http://www.pjms.org.pk/index.php/pjms/article/view/2739">Ahmed et al., 2020</a>)  publicado en el journal de medicina de Pakistán que haya analizado la cuestión y sobre todo, muchas opiniones. Por otro lado, la preocupación por la salud de los fumadores en el caso del COVID-19 tampoco está justificada, ya que si bien es cierto que una vez en el hospital suelen tener peor evolución <a href="https://journals.sagepub.com/doi/10.1177/2040622320935765">(Faraslinos et al., 2020a)</a>, tienen mucha menos probabilidad de entrar en el hospital que el resto de la población (<a href="https://link.springer.com/article/10.1007/s11739-020-02355-7">Farsalinos et al., 2020b</a>, <a href="https://www.medrxiv.org/content/10.1101/2020.06.01.20118877v2">Israel et al., 2020</a>; <a href="https://op.europa.eu/en/publication-detail/-/publication/0d4b3889-046a-11eb-a511-01aa75ed71a1/language-en">Wenzl, 2020</a>)</p>



<p>En España, a raíz de la prohibición del verano pasado de fumar en terrazas de Galicia (tomada sin ninguna evidencia científica) un elevado número de Comunidades Autónomas rápidamente se unió al festival de restricciones. Actualmente, parece ser que la prohibición ya se ha levantado en Galicia, pero permanece en muchas otras.&nbsp;La justificación teórica que motivó esta restricción es que el coronavirus se transmite mediante aerosoles y que el humo exhalado por cigarrillos, e-cigarrillos o vapeadores podría transportar el virus con capacidad infectiva y favorecer la transmisión de la enfermedad.  De momento no hay prueba de ello y en cualquier caso, el riesgo epidémico de permitir/prohibir fumar debe ponerse en contexto con el riesgo de otras actividades respiratorias</p>



<p>Así, hace apenas un mes, un artículo en la revista científica internacional “<em>Journal of Environmental Research and Public Health”</em> elaborado por (<a href="https://www.mdpi.com/1660-4601/18/4/1437">Sussman et al., 2021</a>) muestra (y menciona explícitamente en la discusión de sus resultados) que la prohibición de fumar en las terrazas adoptada en España es absurda y no tiene fundamentos científicos.</p>



<p>En sus palabras:</p>



<p><em>“&#8230; Prohibir el vapeo en espacios al aire libre completamente abiertos aludiendo a la eliminación de la máscara o la posible transmisión de fómites tiene una justificación débil y extremadamente especulativa, especialmente en espacios abiertos como terrazas de restaurantes o al aire libre. &nbsp;Lamentablemente, el Consejo Interterritorial del Sistema Nacional de Salud en España ha invocado precisamente <a href="https://www.mscbs.gob.es/ciudadanos/proteccionSalud/tabaco/docs/Posicionamiento_TyR_COVID19.pdf">en su documento de posicionamiento </a>la necesidad de proteger a la ciudadanía del contagio del COVID-19 sobre bases débiles para justificar una prohibición a nivel nacional de fumar y vapear en todos los espacios exteriores (incluso espacios completamente abiertos)</em>&#8230;</p>



<p><em>&#8230; Las autoridades sanitarias españolas no aportan pruebas empíricas de que se haya producido un contagio real de COVID-19 a través de vapeo o exhalaciones de tabaco ni una justificación técnica coherente que respalde su plausibilidad, pero, sin embargo, invocan el principio de precaución para justificar la aplicación de esta prohibición al menos durante el período de vigencia&#8221;</em></p>



<p>De hecho, los autores del estudio platean mediante un riguroso ejercicio de modelización y calibración de parámetros (para los interesados, ver los detalles <a href="https://www.medrxiv.org/content/10.1101/2020.11.21.20235283v1.full.pdf">aquí</a>) que la actividad de vapear y fumar es una actividad respiratoria relativamente infrecuente e intermitente con una tasa de emisión media de 79.82 gotas (media = 6-200, std = 74.66) que sólo añade un riesgo del 1% al hecho de respirar sin mascarilla en un espacio cerrado, por lo que es prácticamente irrelevante desde el punto de vista de la gestión de la epidemia.</p>



<p>La figura abajo, muestra que, comparado con otras actividades permitidas, el incremento del riesgo de contagio viral asociado al hecho de vapear/fumar es casi nulo. &nbsp;</p>



<figure class="wp-block-image size-large"><a href="https://sistemaencrisis.files.wordpress.com/2021/03/riesgo_fumar_vs_otras_actividades.png"><img data-attachment-id="6310" data-permalink="https://sistemaencrisis.es/riesgo_fumar_vs_otras_actividades/" data-orig-file="https://sistemaencrisis.files.wordpress.com/2021/03/riesgo_fumar_vs_otras_actividades.png" data-orig-size="579,432" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="riesgo_fumar_vs_otras_actividades" data-image-description="" data-image-caption="" data-medium-file="https://sistemaencrisis.files.wordpress.com/2021/03/riesgo_fumar_vs_otras_actividades.png?w=300" data-large-file="https://sistemaencrisis.files.wordpress.com/2021/03/riesgo_fumar_vs_otras_actividades.png?w=579" src="https://sistemaencrisis.files.wordpress.com/2021/03/riesgo_fumar_vs_otras_actividades.png?w=579" alt="" class="wp-image-6310" srcset="https://sistemaencrisis.files.wordpress.com/2021/03/riesgo_fumar_vs_otras_actividades.png 579w, https://sistemaencrisis.files.wordpress.com/2021/03/riesgo_fumar_vs_otras_actividades.png?w=150 150w, https://sistemaencrisis.files.wordpress.com/2021/03/riesgo_fumar_vs_otras_actividades.png?w=300 300w" sizes="(max-width: 579px) 100vw, 579px" /></a></figure>



<p>De hecho, según las estimaciones de estos investigadores, el incremento de riesgo de contagio asociado a fumar es un 43-175% inferior al de hablar 6–24 min por hora y un 259% inferior al de toser cada dos minutos.  Estos resultados aplican igualmente al caso de los cigarrillos convencionales, en los que el 80% de los aerosoles no están relacionados con el sistema respiratorio sino con la quema del cigarro). </p>



<p>Sin embargo, vivimos en el único país del mundo en el que se permite comer y hablar en interiores durante más de media hora  (donde el riesgo de contagio es mucho más elevado) pero se prohíbe fumar en el exterior de las terrazas, incluso cuando hay distancia de seguridad.<br></p>



<p>PD: Dedicado a los técnicos y consejeros de la Generalitat Valenciana y otras tantas regiones. </p>



<p></p>



<p></p>



<p></p>
