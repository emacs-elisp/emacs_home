<ul class="org-ul">
<li><a href="https://git.savannah.gnu.org/cgit/emacs.git/commit/?h=emacs-28&amp;id=2750d97543bce12e99a650d748a4d85343d7d229">The emacs-28 release branch has been created</a> (<a href="https://www.reddit.com/r/emacs/comments/pz3k71/the_emacs28_release_branch_has_been_cut/">Reddit</a>, <a href="https://news.ycombinator.com/item?id=28717552">HN</a>)</li>
<li>Upcoming events:
<ul class="org-ul">
<li>EmacsATX: TBD <a href="https://www.meetup.com/EmacsATX/events/281048454/">https://www.meetup.com/EmacsATX/events/281048454/</a> Wed Oct 6 1630 Vancouver / 1830 Chicago / 1930 Toronto / 2330 GMT &#x2013; Thu Oct 7 0130 Berlin / 0500 Kolkata / 0730 Singapore</li>
<li>M-x Research (contact them for password): TBA <a href="https://m-x-research.github.io/">https://m-x-research.github.io/</a> Fri Oct 15 0700 Vancouver / 0900 Chicago / 1000 Toronto / 1400 GMT / 1600 Berlin / 1930 Kolkata / 2200 Singapore</li>
<li><a href="http://emacsnyc.org/2021/10/04/monthly-online-meetup-lightning-talks.html">Emacs NYC: Monthly Online Meetup - Lightning Talks</a> Nov 1</li>
</ul></li>
<li>Emacs configuration:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/q055n1/one_month_in_init_file_code_review/">One Month in Init File Code Review</a></li>
<li><a href="https://www.youtube.com/watch?v=50Vsh4qw-E4">"Inverse Literate" Emacs Configurations - Hack Sessions</a> (01:09:55)</li>
<li><a href="https://www.youtube.com/watch?v=nXu32MHBDqA">"Inverse Literate" Emacs Configurations (Part 2) - Hack Sessions</a> (43:29)</li>
</ul></li>
<li>Emacs Lisp:
<ul class="org-ul">
<li><a href="https://i.redd.it/1h30gbhm8lq71.png">svg-lib is on ELPA (https://elpa.gnu.org/packages/svg-lib.html)</a> (<a href="https://www.reddit.com/r/emacs/comments/pyee44/svglib_is_on_elpa/">Reddit</a>)</li>
<li><a href="https://ag91.github.io/blog/2021/10/02/moldable-emacs-molds-need-examples-too/">Moldable Emacs: molds need examples too!</a> (<a href="https://www.reddit.com/r/emacs/comments/pzl896/moldable_emacs_molds_need_examples_too/">Reddit</a>)</li>
<li><a href="https://www.youtube.com/watch?v=HyFbIWvmV0A">scimax - data structures in emacs-lisp</a> (25:59)</li>
</ul></li>
<li>Appearance:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/q1kgn9/hlblockmode_now_supports_highlighting_surrounding/">hl-block-mode now supports highlighting surrounding brackets</a></li>
<li><a href="https://github.com/bkaestner/secret-mode.el">secret-mode.el: Display Your Text as Unicode Block Characters</a> (<a href="https://www.reddit.com/r/emacs/comments/pyg33z/secretmodeel_display_your_text_as_unicode_block/">Reddit</a>)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/pweeeb/ann_idlehighlightmode_fast_symbolatpoint/">[ANN] idle-highlight-mode (fast symbol-at-point highlighting) updated</a></li>
<li><a href="https://readingworldmagazine.com/emacs/2021-09-29-how-to-configure-emacs-base16-themes/">yuri tricys: How Can I Configure Emacs Base16 Themes in var_year?</a></li>
<li><a href="https://protesilaos.com/codelog/2021-09-29-modus-themes-1-6-0/">Protesilaos Stavrou: Modus themes 1.6.0 for GNU Emacs</a></li>
</ul></li>
<li>Navigation:
<ul class="org-ul">
<li><a href="https://github.com/karthink/popper">Popper - Tame the flood of buffers in Emacs</a> (<a href="https://www.reddit.com/r/emacs/comments/q0f6yx/popper_tame_the_flood_of_buffers_in_emacs/">Reddit</a>)</li>
<li><a href="https://andreyorst.gitlab.io/posts/2021-09-30-why-is-paredit-is-so-un-emacsy/">Why is Paredit is so un-Emacsy?</a> (<a href="https://www.reddit.com/r/emacs/comments/pyt2nh/why_is_paredit_is_so_unemacsy/">Reddit</a>)</li>
<li><a href="https://github.com/ashok-khanna/parevil">ParEvil - Another set of Evil Keybindings for the Lisp family of languages</a></li>
<li><a href="https://github.com/countvajhula/rigpa">rigpa: Modular editing levels and towers. A metacircular modal interface framework</a></li>
<li><a href="https://emacstil.com/til/2021/09/30/ebuku-bookmarks/">Emacs TIL: Manage Web Bookmarks with Emacs + buku</a></li>
<li><a href="https://www.youtube.com/watch?v=1Lpj2-pepPE">Enjoy Reading Documentation With "Info" in Emacs</a> (08:49)</li>
</ul></li>
<li>Writing:
<ul class="org-ul">
<li><a href="https://i.redd.it/l1mwgqrv93r71.jpg">PSA: sentence-end-double-space</a> (<a href="https://www.reddit.com/r/emacs/comments/q0kmw3/psa_sentenceenddoublespace/">Reddit</a>)</li>
<li><a href="https://git.blast.noho.st/mouse/emacs-leo">emacs-leo: Translate word at point from en, es, fr, it, ch, pt, ru, pl to German</a></li>
</ul></li>
<li>Org Mode:
<ul class="org-ul">
<li><a href="https://orgmode.org/Changes.html">Org 9.5 released</a> (<a href="https://www.reddit.com/r/orgmode/comments/py5z2v/org_95_released/">r/orgmode</a>, <a href="https://www.reddit.com/r/emacs/comments/py5z7n/org_95_released/">r/emacs</a>)</li>
<li><a href="https://www.reddit.com/r/orgmode/comments/q0dsjb/orgmode_needs_your_help/">Org-mode needs your help</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/q0i2nb/what_personal_system_do_you_have_to/">What personal system do you have to sync/backup/secure your org/txt/code files?</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/q027aa/code_to_track_org_habit_streaks/">Code to Track Org Habit streaks</a></li>
<li><a href="https://emacstil.com/til/2021/09/28/yasnippet/">Emacs TIL: Insert Templated Code Blocks with Yasnippet</a></li>
<li><a href="https://systemcrafters.net/publishing-websites-with-org-mode/building-the-site/">Build Your Website with Org Mode - System Crafters</a> (<a href="https://www.reddit.com/r/emacs/comments/pwhplp/build_your_website_with_org_mode/">Reddit</a>)</li>
<li><a href="https://vxlabs.com/2021/09/29/convert-org-mode-files-to-docx-with-cmake-and-pandoc-for-mobile-accessibility/">Convert Org Mode files to docx with CMake and Pandoc for mobile accessibility</a> (<a href="https://www.reddit.com/r/orgmode/comments/pz29b3/convert_org_mode_files_to_docx_with_cmake_and/">Reddit</a>)</li>
<li><a href="https://dindi.garjola.net/zettelkustom.html">Garjola Dindi: My Zettelkustom (with Emacs, of course)</a></li>
<li><a href="https://v.redd.it/aic0hb2oduq71">org-roam-timestamps: keep track of creation and modification times for org-roam-nodes</a> (<a href="https://www.reddit.com/r/OrgRoam/comments/pz86n1/orgroamtimestamps_keep_track_of_creation_and/">r/OrgRoam</a>, <a href="https://www.reddit.com/r/orgmode/comments/pz89pp/orgroamtimestamps_keep_track_of_creation_and/">r/orgmode</a>)</li>
<li><a href="https://irreal.org/blog/?p=10005">Irreal: Spanish Punctuation in Org-mode</a></li>
<li><a href="https://www.youtube.com/watch?v=a4uE36Lb2_I">Emacs Screencast #1: Mein ganzes (wissenschaftliches) Leben in Reintextform.</a> (01:10:51)</li>
</ul></li>
<li>Completion:
<ul class="org-ul">
<li><a href="https://github.com/minad/vertico">[Package of the day] VERTical Interactive COmpletion</a> (<a href="https://www.reddit.com/r/emacs/comments/pzt0lh/package_of_the_day_vertical_interactive_completion/">Reddit</a>)</li>
</ul></li>
<li>Coding:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/q1e2tb/magit_v33_released/">Magit v3.3 released</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/q0hm6k/magit_three_ways_to_discardrevert_commits/">Magit: Three ways to discard/revert commits</a></li>
<li><a href="https://github.com/alejandrogallo/edit-indirect-heredoc">new minor mode: Edit indirect heredoc to do C-c ' in heredocs</a> (<a href="https://www.reddit.com/r/emacs/comments/q0n1hl/new_minor_mode_edit_indirect_heredoc_to_do_cc_in/">Reddit</a>)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/q03imx/ann_kubedocel_kubernetes_api_docs_in_emacs/">[ANN] kubedoc.el - Kubernetes api docs in Emacs</a></li>
<li><a href="http://emacsredux.com/blog/2021/09/29/make-script-files-executable-automatically/">Emacs Redux: Make Script Files Executable Automatically</a></li>
<li><a href="https://www.youtube.com/watch?v=LGPdUkb9JHM">Emacs as a Web Integrated Development Environment</a> (<a href="https://www.reddit.com/r/emacs/comments/pwp7le/emacs_as_a_web_integrated_development_environment/">Reddit</a>)</li>
</ul></li>
<li>Mail and news:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/pzoq42/ann_dankmode_015_major_mode_for_browsing_reddit/">[ANN] dank-mode 0.1.5 - major mode for browsing Reddit</a></li>
<li><a href="https://www.youtube.com/watch?v=Qq6s3PwSwjo">Managing Email in Emacs with mu4e</a> (01:13:41)</li>
</ul></li>
<li>Community:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/pxqvtm/weekly_tips_tricks_c_thread/">Weekly Tips, Tricks, &amp;c. Thread</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/q1d2wa/what_is_the_best_what_the_hell_ill_do_it_in_emacs/">What is the best "What the hell, I'll do it in Emacs" utility you've seen?</a></li>
<li><a href="http://mbork.pl/2021-10-04_Emacs_Lisp_book_-_status_update_and_plans">Marcin Borkowski: Emacs Lisp book - status update and plans</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/pwv97t/the_philosophy_of_emacs/">The Philosophy of Emacs</a></li>
<li><a href="http://metaredux.com/posts/2021/09/28/meta-reduce-2021-2-autumn-begins.html">Meta Redux: Meta Reduce 2021.2: Autumn Begins</a></li>
</ul></li>
<li>Fun:
<ul class="org-ul">
<li><a href="https://github.com/duckwork/frowny.el">frowny.el: Insert frownies in Emacs :(</a> (<a href="https://www.reddit.com/r/emacs/comments/pxcgs0/frownyel_insert_frownies_in_emacs/">Reddit</a>)</li>
<li><a href="https://github.com/md-arif-shaikh/soccer">soccer: Emacs package to get soccer fixtures, results etc</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/pyjgpi/schematics_in_gnu_emacs/">Schematics in GNU Emacs</a></li>
</ul></li>
<li>Other:
<ul class="org-ul">
<li><a href="https://git.chrisbeckstrom.com/chris/tiddler-mode">tiddler-mode: View Tiddlywiki tiddlers in Emacs</a></li>
<li><a href="https://protesilaos.com/codelog/2021-10-02-introducing-tmr-el/">Protesilaos Stavrou: Introducing tmr.el for Emacs</a> - setting a timer</li>
<li><a href="https://emacstil.com/til/2021/10/04/edit-text-everywhere-with-emacs/">Emacs TIL: Edit Text Everywhere with Emacs</a></li>
<li><a href="https://helpdeskheadesk.net/help-desk-head-desk/2021-09-28/">TAONAW: Emacs on MacOS, Part 1</a></li>
</ul></li>
<li>Emacs development:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/pxpq8d/rfc_emacs_treesitter_integration/">RFC: Emacs tree-sitter integration</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=8b4a6a722a3982024fc87b26dbc7ef7e2043f6e1">Add new command 'ensure-empty-lines'.</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=069749bed7ab1587f0cfbadb5924c80d7ee49ac9">Add new user option 'translate-upper-case-key-bindings'</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=39d9b96a606d1c605c329a6c7d1dab6afbd3b824">Remove MozRepl stuff from js.el</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=b421e086cad435977b76e940e7e0bfb8b7db2ac9">Allow computing :doc-spec info-look elements at run time</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=84192b6716f4ee66787ea319bcdb37211eb5add2">New function define-keymap and new macro defvar-keymap</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=344634d7cc5206027fda8e791beef8c43de8aedb">Allow obsoleting themes</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=3c972723e44c9428ea990562033acfbd84ed29d9">* lisp/emacs-lisp/subr-x.el (with-memoization): New macro</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=a2a6c7abcbb5c2776ab5cdbb415df9ad5daa4e13">New command for unmarking all images in image-dired</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=5988d6fa2610097f91d822df813cecbdeed26185">Restore the flet indentation fixes</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=1a653209030279aa03898f647376f768f5d1e9f2">Add new functionality to write buffer-based tests</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=2e92f90a5d38f92f5d4a8a01e28e49f648ef07b4">Save position in mark ring before jumping to definition</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=3478e64c88fe0187f49343ed778d7e9231cf5837">Bump Emacs version to 29.0.50</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=41723a329e5dc7e6d0c0bf07add8852bffcc8f81">Revert "Indent bodies of local function definitions properly in elisp-mode"</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=be493ea67acdc9367e29025382b534ec4339302f">Revert "Fix regressions in cl-flet indentation"</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=164aac0a9919fb34896ddd824394a65802343a50">* etc/NEWS: Announce Org update.</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=dc94ca7b2b878c9a88be72fea118bf6557259ffd">Add new '/wii' convenience ERC command</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=9fc1fdcbf330b0a85cd019bb75afcb8d36243524">Restore the previous order of ERC's '/whois' arguments</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=758753431af51f7ac79a55d426b915443e66a077">New command mpc-goto-playing-song</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=6fecf6ef2552a9b44c4311b7d5af3af0a5a54dbe">Add bindings for 'undo-redo'</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=923b89248cb79d3185264f1175099d549fdaa5bf">Cross reference `dired-do-revert-buffer'</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=1a0f4b3f2352ea5efeac8b3ad704304ed0244f25">Remove rcirc-omit-responses-after-join option</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=902f31d32b61d0e7e73d5429334fa945e2eece37">New user option mpc-cover-image-re</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=081eb52e4d8441a82134db5b34848474a1d01acf">Switch the order of ERC's '/whois' arguments sent to the server</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=cbb0b5d8d5c823357951689ea4f14994e0399992">Unobsolete erc-compat.el</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=b9fa57e5b06a826744fee5b4af9cc45d0ee2ff26">* etc/NEWS: Announce Eshell bookmarks.</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=da89bdde2e3aa941594a112db884ede1beaac658">Add bookmark.el support to eww</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=ee856b76ff6a241cefec1f448a2c5528ad0f1c56">Enable show-paren-mode by default</a></li>
</ul></li>
<li>New packages:
<ul class="org-ul">
<li><a target="_blank" href="https://elpa.gnu.org/packages/multi-mode.html">multi-mode</a>: support for multiple major modes</li>
<li><a target="_blank" href="https://elpa.gnu.org/packages/nano-theme.html">nano-theme</a>: N Λ N O theme</li>
<li><a target="_blank" href="https://elpa.gnu.org/packages/svg-lib.html">svg-lib</a>: SVG tags, progress bars &amp; icons</li>
<li><a target="_blank" href="http://melpa.org/#/consult-company">consult-company</a>: Consult frontend for company</li>
<li><a target="_blank" href="http://melpa.org/#/consult-yasnippet">consult-yasnippet</a>: A consulting-read interface for yasnippet</li>
<li><a target="_blank" href="http://melpa.org/#/emacsql-libsqlite3">emacsql-libsqlite3</a>: EmacSQL back-end for SQLite using a module</li>
<li><a target="_blank" href="http://melpa.org/#/flycheck-php-noverify">flycheck-php-noverify</a>: Flycheck checker for PHP Noverify linter</li>
<li><a target="_blank" href="http://melpa.org/#/kubedoc">kubedoc</a>: Kubernetes API Documentation</li>
<li><a target="_blank" href="http://melpa.org/#/org-auto-expand">org-auto-expand</a>: Automatically expand certain headings</li>
<li><a target="_blank" href="http://melpa.org/#/text-categories">text-categories</a>: Assign text categories to a buffer for mass deletion</li>
</ul></li>
</ul>

<p>
Links from <a href="https://www.reddit.com/r/emacs">reddit.com/r/emacs</a>, <a href="https://www.reddit.com/r/orgmode">r/orgmode</a>, <a href="https://www.reddit.com/r/spacemacs">r/spacemacs</a>, <a href="https://www.reddit.com/r/planetemacs">r/planetemacs</a>, <a href="https://hn.algolia.com/?query=emacs&amp;sort=byDate&amp;prefix&amp;page=0&amp;dateRange=all&amp;type=story">Hacker News</a>, <a href="https://planet.emacslife.com">planet.emacslife.com</a>, <a href="https://www.youtube.com/playlist?list=PL4th0AZixyREOtvxDpdxC9oMuX7Ar7Sdt">YouTube</a>, <a href="http://git.savannah.gnu.org/cgit/emacs.git/log/etc/NEWS">the Emacs NEWS file</a>, <a href="https://emacslife.com/calendar/">Emacs Calendar</a> and <a href="http://lists.gnu.org/archive/html/emacs-devel/2021-10">emacs-devel</a>.</p>
