<p>
I thought it was time to give <a href="https://masteringemacs.org/article/complete-guide-mastering-eshell">eshell</a> in Emacs another try. It has some
pretty cool features but for whatever reason, I&#39;ve never really been
able to adopt Emacs as my go to shell.</p>
<p>
Eshell out of the box is pretty cool but could use some
enhancements. When launching at login it doesn&#39;t know about the path
you set in your .bashrc or .zshrc in my case files. It just seemed to
have problems with paths in general but that was fixed with the
exce-path-from-shell package. The prompt also needed some fixing up
along with some other tweaks. </p>
<p>
I found <a href="https://github.com/manateelazycat/aweshell">aweshell</a> which looked promising but it wasn&#39;t on melpa so I
had to clone it separately. I also noticed that it basically tied
together some packages I could download myself and added a shell
switcher but didn&#39;t bind the keys.</p>
<p>
I thought I&#39;d dive into elisp -something I haven&#39;t done in a while to
write my own.</p>
<p>
Here&#39;s my current complete eshell config:</p>
<p>
<div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-elisp" data-lang="elisp">(use-package exec-path-from-shell
  :ensure <span style="color:#66d9ef">t</span>
  :config
  (exec-path-from-shell-initialize))


    (use-package fish-completion
    :ensure <span style="color:#66d9ef">t</span>
    :config
    (global-fish-completion-mode))
  <span style="color:#75715e">;; (use-package eshell-prompt-extras </span>
  <span style="color:#75715e">;; :ensure t</span>
  <span style="color:#75715e">;; :config</span>
  <span style="color:#75715e">;; (setq epe-show-python-info nil)</span>
  <span style="color:#75715e">;; )</span>

  (use-package eshell-git-prompt
  :ensure <span style="color:#66d9ef">t</span>
  :config
  (eshell-git-prompt-use-theme <span style="color:#e6db74">&#39;git-radar</span>)
  )</code></pre></div></p>
<p>
And here&#39;s the code I ended up with for my shell switcher:</p>
<p>
<div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-elisp" data-lang="elisp"><span style="color:#960050;background-color:#1e0010">#</span>+BEGIN_SRC emacs-lisp
  (require <span style="color:#e6db74">&#39;cl-lib</span>)
  (defun select-or-create (arg)
    <span style="color:#e6db74">&#34;Commentary ARG.&#34;</span>
    (if (string= arg <span style="color:#e6db74">&#34;New eshell&#34;</span>)
        (eshell <span style="color:#66d9ef">t</span>)
      (switch-to-buffer arg)))
  (defun eshell-switcher (<span style="color:#66d9ef">&amp;optional</span> arg)
    <span style="color:#e6db74">&#34;Commentary ARG.&#34;</span>
    (interactive)
    (let* (
           (buffers (cl-remove-if-not (lambda (n) (<span style="color:#a6e22e">eq</span> (<span style="color:#a6e22e">buffer-local-value</span> <span style="color:#e6db74">&#39;major-mode</span> n) <span style="color:#e6db74">&#39;eshell-mode</span>)) (<span style="color:#a6e22e">buffer-list</span>)) )
           (names (<span style="color:#a6e22e">mapcar</span> (lambda (n) (<span style="color:#a6e22e">buffer-name</span> n)) buffers))
           (num-buffers (<span style="color:#a6e22e">length</span> buffers) )
           (in-eshellp (<span style="color:#a6e22e">eq</span> major-mode <span style="color:#e6db74">&#39;eshell-mode</span>)))
      (cond ((<span style="color:#a6e22e">eq</span> num-buffers <span style="color:#ae81ff">0</span>) (eshell (or arg <span style="color:#66d9ef">t</span>)))
            ((not in-eshellp) (switch-to-buffer (<span style="color:#a6e22e">car</span> buffers)))
            (<span style="color:#66d9ef">t</span> (select-or-create (<span style="color:#a6e22e">completing-read</span> <span style="color:#e6db74">&#34;Select Shell:&#34;</span> (<span style="color:#a6e22e">cons</span> <span style="color:#e6db74">&#34;New eshell&#34;</span> names)))))))</code></pre></div></p>
<p>
I currently bound eshell-switcher to <code>CTRL-z e</code>. </p>
<p>
The video goes through the whole process:</p>
<p>
&lt;iframe width=&#34;560&#34; height=&#34;315&#34;
src=&#34;<a href="https://www.youtube.com/embed/-dIjFZBDt64">https://www.youtube.com/embed/-dIjFZBDt64</a>&#34; frameborder=&#34;0&#34;
allow=&#34;accelerometer; autoplay; encrypted-media; gyroscope;
picture-in-picture&#34; allowfullscreen&gt;&lt;/iframe&gt;</p>
