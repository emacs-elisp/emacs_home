<ul class="org-ul">
<li>Ending soon: <a href="https://emacsconf.org/2021/cfp/">EmacsConf 2021 Call for Proposals</a> (<a href="https://reddit.com/r/emacs/comments/oyl40m/emacsconf_2021_call_for_proposals/">Reddit</a>, <a href="https://news.ycombinator.com/item?id=28075442">HN</a>) until Sept 30</li>
<li>Upcoming events:
<ul class="org-ul">
<li>EmacsConf Office Hour <a href="https://emacsconf.org/2021/office-hours/">https://emacsconf.org/2021/office-hours/</a> Tue Sep 28 1800 Vancouver / 2000 Chicago / 2100 Toronto &#x2013; Wed Sep 29 0100 GMT / 0300 Berlin / 0630 Kolkata / 0900 Singapore</li>
<li>Emacs Berlin (virtual, in English) <a href="https://emacs-berlin.org/">https://emacs-berlin.org/</a> Wed Sep 29 0930 Vancouver / 1130 Chicago / 1230 Toronto / 1630 GMT / 1830 Berlin / 2200 Kolkata &#x2013; Thu Sep 30 0030 Singapore</li>
<li>M-x Research (contact them for password): TBA <a href="https://m-x-research.github.io/">https://m-x-research.github.io/</a> Fri Oct 1 0700 Vancouver / 0900 Chicago / 1000 Toronto / 1400 GMT / 1600 Berlin / 1930 Kolkata / 2200 Singapore</li>
<li>EmacsNYC: Discussion: Remote Collaboration Software and crdt.el <a href="https://www.meetup.com/New-York-Emacs-Meetup/events/280981454/">https://www.meetup.com/New-York-Emacs-Meetup/events/280981454/</a> Mon Oct 4 1600 Vancouver / 1800 Chicago / 1900 Toronto / 2300 GMT &#x2013; Tue Oct 5 0100 Berlin / 0430 Kolkata / 0700 Singapore (<a href="http://emacsnyc.org/2021/09/23/online-meetup-discussionremote-collaboration-software-and-crtd-el.html">Emacs NYC</a>)</li>
<li>EmacsATX: TBD <a href="https://www.meetup.com/EmacsATX/events/hkckgsyccnbjb/">https://www.meetup.com/EmacsATX/events/hkckgsyccnbjb/</a> Wed Oct 6 1630 Vancouver / 1830 Chicago / 1930 Toronto / 2330 GMT &#x2013; Thu Oct 7 0130 Berlin / 0500 Kolkata / 0730 Singapore</li>
</ul></li>
<li>Emacs configuration:
<ul class="org-ul">
<li><a href="https://tech.toryanderson.com/2021/09/20/swapping-chords-in-dvorak-or-why-does-emacs-keyboard-translate-fail-with-wrong-type-argument-characterp-134217845/">Tory Anderson: Swapping chords in Dvorak, or Why does emacs keyboard-translate fail with (wrong-type-argument characterp 134217845)</a></li>
<li><a href="https://tech.toryanderson.com/2021/09/23/fail-fast-transients-like-hydra/">Tory Anderson: Fail-fast Transients like Hydra</a></li>
<li><a href="https://www.youtube.com/watch?v=ZolOUgXHgbo">System Crafters Live! - Investigating use-package Alternatives</a> (02:08:23)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/pthma9/just_out_of_curiosity_how_many/">Just out of curiosity, how many bytes/kilobytes/megabytes does your dot file weight?</a></li>
<li><a href="https://github.com/alphapapa/ap.el">ap.el: A simple, Emacs Lisp-focused Emacs config</a> (<a href="https://www.reddit.com/r/emacs/comments/pt4psr/apel_a_simple_emacs_lispfocused_emacs_config/">Reddit</a>)</li>
<li><a href="https://github.com/alphapapa/with-emacs.sh">with-emacs.sh: Script to easily run Emacs with specified configurations</a> (<a href="https://www.reddit.com/r/emacs/comments/pv46oi/withemacssh_script_to_easily_run_emacs_with/">Reddit</a>)</li>
<li><a href="https://www.youtube.com/watch?v=knRI1O_XSeY">My Current Emacs Configuration</a> (34:33) - web dev, software dev - Timothy Unkert</li>
<li><a href="https://github.com/Crandel/home/tree/master/.config/emacs">Crandel's .emacs</a></li>
</ul></li>
<li>Emacs Lisp:
<ul class="org-ul">
<li><a href="https://countvajhula.com/2021/09/25/symex-el-edit-lisp-code-in-a-vim-like-way/">Sid Kasivajhula: Symex.el: Edit Lisp Code in a Vim-like Way</a></li>
<li><a href="https://countvajhula.com/2021/09/25/the-animated-guide-to-symex/">Sid Kasivajhula: The Animated Guide to Symex</a> (<a href="https://www.reddit.com/r/emacs/comments/pv231f/the_animated_guide_to_symex/">Reddit</a>)</li>
<li><a href="https://www.youtube.com/watch?v=kk2W9krvNVg">scimax - formatting strings in elisp</a> (22:35, <a href="https://irreal.org/blog/?p=9993">Irreal</a>)</li>
<li><a href="https://www.youtube.com/watch?v=t-IsXzEz7hs">scimax - functions in emacs-lisp</a> (26:02, <a href="https://irreal.org/blog/?p=10000">Irreal</a>)</li>
<li><a href="https://youtu.be/yiS1eXdgcYI">Little Experiment - Source Code View in Built-in Help, a lá Helpful</a> (<a href="https://www.reddit.com/r/emacs/comments/pt3icl/little_experiment_source_code_view_in_builtin/">Reddit</a>)</li>
<li><a href="https://lists.gnu.org/archive/html/emacs-devel/2021-09/msg01492.html">Mattias Engdegård makes an interesting discovery about the Emacs Lisp bytecode interpreter, and ERT tests</a> (<a href="https://www.reddit.com/r/emacs/comments/ps5o04/mattias_engdeg%C3%A5rd_makes_an_interesting_discovery/">Reddit</a>)</li>
</ul></li>
<li>Appearance:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/pweeeb/ann_idlehighlightmode_fast_symbolatpoint/">[ANN] idle-highlight-mode (fast symbol-at-point highlighting) updated</a></li>
<li><a href="https://tech.toryanderson.com/2021/09/24/replacing-beacon.el-with-hl-line-flash/">Tory Anderson: Replacing beacon.el with hl-line-flash</a></li>
<li><a href="https://readingworldmagazine.com/emacs/2021-09-22-how-to-configure-emacs-themes/">yuri tricys: How to Configure Emacs Themes in var_year</a></li>
<li><a href="https://github.com/mohkale/an-old-hope-emacs">an-old-hope-emacs: a syntax theme from a galaxy far far away&#x2026;</a></li>
<li><a href="https://www.youtube.com/watch?v=n920BQM_ic4">Emacs Beautification Using Mode-Line Dashboard And Themes</a> (12:14)</li>
</ul></li>
<li>Navigation:
<ul class="org-ul">
<li><a href="https://www.wisdomandwonder.com/programming/13521/automatically-open-read-only-files-in-view-mode?utm_source=rss&amp;utm_medium=rss&amp;utm_campaign=automatically-open-read-only-files-in-view-mode">Grant Rettke: Automatically open read-only files in View mode</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/pt8kod/gumshoe_20_my_first_package_in_melpa/">Gumshoe 2.0, my first package in Melpa</a> - fancy point logger to help you remember where you've been</li>
<li><a href="https://www.youtube.com/watch?v=JkRQ_WWJqyg">Swiper Search &amp; Centaur-Tabs in Emacs</a> (03:56)</li>
<li><a href="https://www.youtube.com/watch?v=-KrMaLq8Bms&amp;feature=youtu.be">Emacs filetree package demo</a> (<a href="https://www.reddit.com/r/emacs/comments/psbsve/demo_video_for_filetree_package/">Reddit</a>)</li>
<li><a href="https://tech.toryanderson.com/2021/09/20/the-lost-art-of-page-breaks-in-text-documents/">Tory Anderson: The lost art of page breaks in text documents</a></li>
</ul></li>
<li>Dired:
<ul class="org-ul">
<li><a href="https://ag91.github.io/blog/2021/09/23/moldable-emacs-extending-the-playground-powers-via-hooks-to-include-dired/">Moldable Emacs: extending the Playground powers via hooks (to include Dired)</a> (<a href="https://www.reddit.com/r/emacs/comments/pu3v7r/moldable_emacs_extending_the_playground_powers/">Reddit</a>)</li>
</ul></li>
<li>Org Mode:
<ul class="org-ul">
<li><a href="https://www.youtube.com/watch?v=NZZVRCo1dLc">Emacs Org-Mode Intro</a> (13:14)</li>
<li><a href="https://www.youtube.com/watch?v=ffBDQauDAgQ">O que é, e como utilizar: Org em Emacs</a> (22:48)</li>
<li><a href="https://github.com/alphapapa/org-ql#06">[ANN] org-ql 0.6 released</a> (<a href="https://www.reddit.com/r/emacs/comments/pt1iol/ann_orgql_06_released/">Reddit</a>)</li>
<li><a href="https://helpdeskheadesk.net/help-desk-head-desk/2021-09-19/">TAONAW: Refreshing my Capture Templates</a> (<a href="https://www.reddit.com/r/orgmode/comments/pt665p/capture_templates_updated_mine_whats_yours/">Reddit</a>)</li>
<li>Images:
<ul class="org-ul">
<li><a href="https://i.redd.it/b81lkn1sd4p71.gif">Now you can real-ly view your org files</a> (<a href="https://www.reddit.com/r/orgmode/comments/ptgfhs/now_you_can_really_view_your_org_files/">Reddit</a>)</li>
<li><a href="https://junjizhi.com/all/experience/2021/09/19/emacs-systems-thinking/">Use Emacs for Systems Thinking</a> (<a href="https://irreal.org/blog/?p=9996">Irreal</a>)</li>
<li><a href="https://github.com/misohena/el-easydraw">Emacs Drawing Tool</a> (<a href="https://www.reddit.com/r/emacs/comments/pvtbq5/emacs_drawing_tool/">Reddit</a>) - for drawing diagrams in Org, includes color picker</li>
</ul></li>
<li>Org Roam:
<ul class="org-ul">
<li><a href="https://www.badykov.com/emacs/2021/09/27/split/">I split my brain</a> (<a href="https://www.reddit.com/r/emacs/comments/pvrv0u/i_split_my_brain/">Reddit</a>) - org-roam contexts</li>
<li><a href="https://www.youtube.com/watch?v=CUkuyW6hr18">5 Org Roam Hacks for Better Productivity in Emacs</a> (29:44)</li>
</ul></li>
<li>Import, export, and integration:
<ul class="org-ul">
<li><a href="https://www.youtube.com/watch?v=oacHbFO71v8">😎😎 Emacs exportar .org a .pdf (video random)😜😜</a> (14:05)</li>
<li><a href="https://oylenshpeegul.gitlab.io/blog/posts/20210921/">Tim Heaney: blogpost</a> - Rust script to create an ox-hugo blog post</li>
<li><a href="https://emacs.love/weblorg/posts/v0-1-2-slowly-but-surely.html">weblorg - v0.1.2 - Slowly but Surely-weblorg, the modern Static HTML Generator for Emacs and Org-Mode</a></li>
<li><a href="https://www.hoowl.se/khalel.html">Managing calendar events in Emacs</a></li>
</ul></li>
</ul></li>
<li>Coding:
<ul class="org-ul">
<li><a href="https://github.com/astoff/comint-mime">comint-mime: Display graphics and other MIME attachments in Emacs shells</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/pu3mtc/announcing_experimental_support_for_janet/">Announcing experimental support for Janet programming language in PEL</a> (Pragmatic Emacs Library, configuration)</li>
<li><a href="https://github.com/Shopify/theme-check/wiki/Emacs">theme-check on Emacs. Theme Check is a command line tool that helps you follow Shopify Themes &amp; Liquid best practices by analyzing the Liquid &amp; JSON inside your theme</a></li>
<li><a href="https://oylenshpeegul.gitlab.io/blog/posts/20210920/">Tim Heaney: Zig</a></li>
<li><a href="https://www.youtube.com/watch?v=GC0swmxh8AM">EmacsでのISLispモードが公開されています。</a> (06:21)</li>
</ul></li>
<li>Mail and news:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/pvpb6d/mu4e_very_fast_thread_folding/">Mu4e (very) fast thread folding</a></li>
<li><a href="https://github.com/john2x/dank-mode">dank-mode: Emacs major mode for browsing Reddit</a></li>
<li><a href="http://emacsnyc.org/2021/09/20/managing-email-in-emacs-with-mu4e.html">Emacs NYC: Managing Email in Emacs with mu4e</a> (1:13:41)</li>
</ul></li>
<li>Chat:
<ul class="org-ul">
<li><a href="https://zevlg.github.io/telega.el/">Telega Manual (v0.7.018). telega is full featured unofficial client for Telegram platform for GNU Emacs.</a></li>
<li><a href="https://tsdh.org/posts/2021-09-22-rcirc-hack-of-the-day-print-timestamps-less-often.html">Tassilo Horn: Rcirc hack of the day: print timestamps less often</a></li>
</ul></li>
<li>Community:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/pt2xws/weekly_tips_tricks_c_thread/">Weekly Tips, Tricks, &amp;c. Thread</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/puqodm/xemacs_vs_gnu_emacs/">XEmacs vs. GNU Emacs</a></li>
<li><a href="https://i.redd.it/qpbqimfqx4p71.jpg">Life of an Emacser</a> (<a href="https://www.reddit.com/r/emacs/comments/pti06k/life_of_an_emacser/">Reddit</a>)</li>
<li><a href="https://protesilaos.com/codelog/2021-09-22-live-stream-emacs-unix/">Protesilaos Stavrou: Live stream: Emacs and the Unix philosophy</a></li>
<li><a href="https://alexschroeder.ch/wiki/2021-09-22_The_first_programming_language_is_English">Alex Schroeder: The first programming language is English</a></li>
<li><a href="https://lars.ingebrigtsen.no/2021/09/21/11x10/">Lars Ingebrigtsen: 11x10%</a></li>
</ul></li>
<li>Other:
<ul class="org-ul">
<li><a href="https://www.youtube.com/watch?v=ub3T-y6_u-4">Como utilizar o Emacs (com evil mode)</a> (27:59)</li>
<li><a href="https://www.youtube.com/watch?v=b3Ja6N0edwo">Emacs Macros</a> (07:43) - keyboard macros</li>
<li><a href="https://www.youtube.com/watch?v=PE714pm1kvE">System Crafters Live! - The Many Varieties of Emacs</a> (48:26)</li>
<li><a href="https://www.youtube.com/watch?v=JMP8JjmS3ds">System Crafters Live! - The Many Varieties of Emacs (Part 2)</a> (1:07:41)</li>
<li><a href="https://i.redd.it/bg5uuk1d71p71.png">I love that you can do this (A few of them got a little destroyed)</a> (<a href="https://www.reddit.com/r/emacs/comments/pt4lh1/i_love_that_you_can_do_this_a_few_of_them_got_a/">Reddit</a>) - running various text editors inside Emacs</li>
<li><a href="https://github.com/ruediger/qrencode-el">qrencode-el: QRCode encoder for Emacs in pure elisp</a></li>
<li><a href="https://connorberry.com/2021/08/20/emacs-typing-tutor/">Emacs Typing Tutor | Actually</a> (<a href="https://news.ycombinator.com/item?id=28597994">HN</a>)</li>
</ul></li>
<li>Emacs development:
<ul class="org-ul">
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=0f4b55dc8d551f266ae8cc36d94bcd216a80754b">'glyphless-char-display-control' now applies to Variation Selectors.</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=a2a62f71051f1295492780f320e9b7bc02b6e6f4">Enhance font_range to check for emoji composition triggers</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=58055b5fc330689234cafb51398844f6e5791077">Document shorthands in the Elisp manual section on Symbols</a> (<a href="https://www.reddit.com/r/emacs/comments/pwcw7t/jo%C3%A3o_t%C3%A1vora_document_shorthands_in_the_elisp/">Reddit</a>, <a href="https://lists.gnu.org/archive/html/emacs-devel/2021-09/msg01949.html">emacs-devel</a>) - namespacing</li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=7cb29440433bda1ad8defad70cbd43fb2f9f4d1f">Add new macro with-environment-variables</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=fcca1db9fd1eb2930f29972e7def0936c3269f1b">* lisp/tab-line.el: Add modified-buffer face</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=ed02b88bbae18caad650d76876940ffb58cab554">Renege on anonymous &amp;rest (bug#50268, bug#50720)</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=9bc2ac4a95ed2c1d6d7b56c3a1fd72e5ee1dc5f5">Add new user option ispell-help-timeout</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=f1071bf08e246d0820edfb66163acb65e90d9482">New :type natnum for defcustom</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=091791933704cd706f90c1685ac4b35f51c98199">Add support for man page hyperlinks in doc strings</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=38037e04cb05cb1f2b604f0b1602d36b0bcf6985">Indent bodies of local function definitions properly in elisp-mode</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=2abf143f8185fced544c4f8d144ea710142d7a59">New thing-at-point target 'string' used in context-menu-region</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=d4a6e42e92ee215659d09b0456032714aab73ca5">Add support for "bright" ANSI colors in term-mode</a> and <a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=ceb9da3b7125fbdf0da04a3b158ac1e792c87f4f">ansi-color</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=aef84c5f17c33714bda402e9408a3cb2ae928b61">Add aggregate project discovery and maintenance functions</a>: project-remember-projects-under, project-forget-project, project-forget-projects-under, project-forget-zombie-projects</li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=efdb89f15b4d0dae334952bfe11073534e244d75">Move 'kdb-macro-redisplay' key binding</a> (old: C-x C-k Q, new: C-x C-k d)</li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=2386b085268af9c06aebc5c4aced1aa6a0d3f702">Fix build with native compilation but without zlib</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=7b62ea95c9616b6897b8a727a219925c62c38557">New major mode with font-locking for etc/AUTHORS</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=be4f8584983e63905aa409efad11fb7d8d418ccb">Add new function `ensure-list'</a>: makes a list of its object if it's not a list already</li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=1c73c0b33a9b10cdae1316ad9e0ba861af454b66">Add new command 'checkdoc-dired'</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=14495e33afa0b8038c494f1e6e90065683ccbd07">Consistently test alist keys with equal in map.el</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=5b962a7ad8d0acfe40a41ce139059b9c8e46f666">Avoid double argument evaluation in vc-call macro</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=7e395a59b025c7f4be49294ad806addf5b1a25c9">Make dired-compress-file query for a directory to uncompress to</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=9767c6b01dccb7e29e240a22d978835d240e8ab0">New command 'project-find-dir' runs Dired in a directory inside project</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=de289d58a4efff4a8625f100eabdc56da98e2e07">Support for Unicode emoji sequences</a></li>
</ul></li>
<li>New packages:
<ul class="org-ul">
<li><a target="_blank" href="http://melpa.org/#/lacquer">lacquer</a>: Switch theme/font by selecting from a cache</li>
<li><a target="_blank" href="http://melpa.org/#/org-movies">org-movies</a>: Manage watchlist with Org mode</li>
</ul></li>
</ul>

<p>
Links from <a href="https://www.reddit.com/r/emacs">reddit.com/r/emacs</a>, <a href="https://www.reddit.com/r/orgmode">r/orgmode</a>, <a href="https://www.reddit.com/r/spacemacs">r/spacemacs</a>, <a href="https://www.reddit.com/r/planetemacs">r/planetemacs</a>, <a href="https://hn.algolia.com/?query=emacs&amp;sort=byDate&amp;prefix&amp;page=0&amp;dateRange=all&amp;type=story">Hacker News</a>, <a href="https://planet.emacslife.com">planet.emacslife.com</a>, <a href="https://www.youtube.com/playlist?list=PL4th0AZixyREOtvxDpdxC9oMuX7Ar7Sdt">YouTube</a>, <a href="http://git.savannah.gnu.org/cgit/emacs.git/log/etc/NEWS">the Emacs NEWS file</a>, <a href="https://emacslife.com/calendar/">Emacs Calendar</a> and <a href="http://lists.gnu.org/archive/html/emacs-devel/2021-09">emacs-devel</a>.
</p>
