<div id="wpbody"> First of all, what is a loop? A loop is a construct that executes statements repeatedly until a specified condition is met. The statements are in the block of the loop. In C++, if there is only one statement to be repeated, then there will be no block (braces). There is the do-while loop, while-loop, and the for-loop. Such constructs are called compound statements.</p>
<h2>do-while loop</h2>
<p>A simple do-while loop is:</p>
<div class="codecolorer-container cpp blackboard" style="border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">int</span> m <span style="color: #000080;">=</span> <span style="color: #0000dd;">0</span><span style="color: #008080;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">do</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#123;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000dd;">cout</span> <span style="color: #000080;">&lt;&lt;</span> m <span style="color: #000080;">&lt;&lt;</span> <span style="color: #FF0000;">' '</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #000040;">++</span>m<span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#125;</span> <span style="color: #0000ff;">while</span> <span style="color: #008000;">&#40;</span>m<span style="color: #000080;">&lt;</span><span style="color: #0000dd;">5</span><span style="color: #008000;">&#41;</span><span style="color: #008080;">;</span></div></div>
<p>There is an initial condition that is not really part of the loop. This initial condition is “int m = 0;”. The loop begins with the reserved word, do, and ends with a semicolon, after the while condition, “(m&lt;5)”. The loop means to print out integers, beginning from zero until m is equal to 5. When m is equal to 5, no printing takes place.</p>
<p>With the do-while loop, the block is executed first, before the condition is checked. The condition is met when m is equal to 5. Before then, the block is executed 5 times, beginning from a count from 0 to 4. So the output is:</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; <span style="color: #0000dd;">0</span> <span style="color: #0000dd;">1</span> <span style="color: #0000dd;">2</span> <span style="color: #0000dd;">3</span> <span style="color: #0000dd;">4</span></div></div>
<h2>while-loop</h2>
<p>With the do-while loop, the block is executed each time before the condition is checked. On the other hand, with the while-loop, the condition is checked first, each time, before the block is executed. The following while-loop does what the above do-while loop has done:</p>
<div class="codecolorer-container cpp blackboard" style="border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">int</span> m <span style="color: #000080;">=</span> <span style="color: #0000dd;">0</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; <br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="color: #0000ff;">while</span> <span style="color: #008000;">&#40;</span>m<span style="color: #000080;">&lt;</span><span style="color: #0000dd;">5</span><span style="color: #008000;">&#41;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#123;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000dd;">cout</span> <span style="color: #000080;">&lt;&lt;</span> m <span style="color: #000080;">&lt;&lt;</span> <span style="color: #FF0000;">' '</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #000040;">++</span>m<span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#125;</span></div></div>
<p>There is an initial condition that is not really part of the loop. This initial condition is “int m = 0;”. The loop begins with the reserved word and ends at the second and closing brace for its block. There is no “do” reserved word here. The while condition is “(m&lt;5)”, same as in the above code. When the while-condition is false, that is, m equals 5, the block is not executed, and that is the end of the loop. Note that the loop does not end with a semicolon in this case. The output is the same as in the previous code, that is:</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; <span style="color: #0000dd;">0</span> <span style="color: #0000dd;">1</span> <span style="color: #0000dd;">2</span> <span style="color: #0000dd;">3</span> <span style="color: #0000dd;">4</span></div></div>
<h2>for-loop</h2>
<p>In the above while-loop, there are five main components. The first one is the initialization statement,</p>
<div class="codecolorer-container cpp blackboard" style="border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">int</span> m <span style="color: #000080;">=</span> <span style="color: #0000dd;">0</span><span style="color: #008080;">;</span></div></div>
<p>Repetition of the loop block execution takes place for 5 numbers, beginning when m is zero. The next component is the while condition (m&lt;5). No repetition (no execution of the block) occurs when the while condition results in false. The third component is the loop compound statement itself, which can be imagined as the block. The fourth component is inside the loop compound statement. It is:</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000dd;">cout</span> <span style="color: #000080;">&lt;&lt;</span> m <span style="color: #000080;">&lt;&lt;</span> <span style="color: #FF0000;">' '</span><span style="color: #008080;">;</span></div></div>
<p>It prints the value of m. And the fifth component is the increment statement,</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #000040;">++</span>m<span style="color: #008080;">;</span></div></div>
<p>Which is in the block, at the end. This causes repetition of the execution of the block. It introduces the next number for repeat execution of the block. However, repetition will not take place if the while condition results in false.</p>
<p>The different five components can be rearranged in a for-loop as follows:</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="color: #0000ff;">for</span> <span style="color: #008000;">&#40;</span><span style="color: #0000ff;">int</span> m<span style="color: #000080;">=</span><span style="color: #0000dd;">0</span><span style="color: #008080;">;</span> m<span style="color: #000080;">&lt;</span><span style="color: #0000dd;">5</span><span style="color: #008080;">;</span> <span style="color: #000040;">++</span>m<span style="color: #008000;">&#41;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#123;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000dd;">cout</span> <span style="color: #000080;">&lt;&lt;</span> m <span style="color: #000080;">&lt;&lt;</span> <span style="color: #FF0000;">' '</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#125;</span></div></div>
<p>The output is the same, i.e.</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; <span style="color: #0000dd;">0</span> <span style="color: #0000dd;">1</span> <span style="color: #0000dd;">2</span> <span style="color: #0000dd;">3</span> <span style="color: #0000dd;">4</span></div></div>
<p>There are three statements in the parentheses of the for-loop: there is the initialization statement, there is the while condition, which is a short statement, and there is the increment statement. In the block, this time, there is only one statement, which is the core statement. The increment statement, which was in the block, has been taken up into the parentheses. More statements can be added to any of the above blocks.</p>
<p>The do-while loop and the while loop are fundamentally the same as the for-loop. The for-loop puts looping in a classical and simplified form. Any loop needs an initialization state, a while condition, and code to cause the next repetition, which will not occur when the while condition results in false. </p>
<p>When nesting a loop, these three features should be taken into consideration. This article explains different ways of nesting loops in C++. The basic loop forms have been explained above.</p>
<h2>Article Content</h2>
<ul>
<li><a href="#1">Nesting do-while Loops</a></li>
<li><a href="#2">Nesting while-Loops</a></li>
<li><a href="#3">Nesting for-Loops</a></li>
<li><a href="#4">Conclusion</a></li>
</ul>
<h2 id="1">Nesting do-while Loops</h2>
<p>From the following program, a table of 5 rows and 4 columns of characters are displayed. In C++, the sequence of uppercase letters or the sequence of lowercase letters can each be compared as whole numbers are compared, as illustrated in the following nested loop:</p>
<div class="codecolorer-container cpp blackboard" style="border:1px solid #9F9F9F;width:435px;height:100%;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; <span style="color: #339900;">#include &lt;iostream&gt;</span><br />
&nbsp; &nbsp; <span style="color: #0000ff;">using</span> <span style="color: #0000ff;">namespace</span> std<span style="color: #008080;">;</span><br />
<br />
&nbsp; &nbsp; <span style="color: #0000ff;">int</span> main<span style="color: #008000;">&#40;</span><span style="color: #008000;">&#41;</span><br />
&nbsp; &nbsp; <span style="color: #008000;">&#123;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">int</span> i <span style="color: #000080;">=</span> <span style="color: #0000dd;">0</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">char</span> j <span style="color: #000080;">=</span> <span style="color: #FF0000;">'A'</span><span style="color: #008080;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">do</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#123;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">do</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#123;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000dd;">cout</span> <span style="color: #000080;">&lt;&lt;</span> j <span style="color: #000080;">&lt;&lt;</span> <span style="color: #FF0000;">' '</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; j<span style="color: #000040;">++</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#125;</span> <span style="color: #0000ff;">while</span> <span style="color: #008000;">&#40;</span>j <span style="color: #000080;">&lt;</span> <span style="color: #FF0000;">'E'</span><span style="color: #008000;">&#41;</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; j <span style="color: #000080;">=</span> <span style="color: #FF0000;">'A'</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000dd;">cout</span> <span style="color: #000080;">&lt;&lt;</span> endl<span style="color: #008080;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; i<span style="color: #000040;">++</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#125;</span> <span style="color: #0000ff;">while</span> <span style="color: #008000;">&#40;</span>i <span style="color: #000080;">&lt;</span> <span style="color: #0000dd;">5</span><span style="color: #008000;">&#41;</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; <br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">return</span> <span style="color: #0000dd;">0</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; <span style="color: #008000;">&#125;</span></div></div>
<p>The output is:</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; A B C D <br />
&nbsp; &nbsp; A B C D <br />
&nbsp; &nbsp; A B C D <br />
&nbsp; &nbsp; A B C D <br />
&nbsp; &nbsp; A B C D</div></div>
<p>The first line of the program includes the iostream library into the program. This library is necessary for printing out text. The next line is a statement, ensuring that any name used is from the standard namespace unless otherwise indicated.</p>
<p>It is helpful to think of a nested loop in two dimensions. Both loops act on each cell. The inner loop is responsible for the columns, while the outer loop is responsible for the rows.</p>
<p>It goes like this: While the outer loop points to the first row, the inner loop prints one cell value, per column, for the first row. While the outer loop points to the second row, the inner loop prints one cell value, per column, for the second row. This continues until the last row is printed.</p>
<p>In the above program, the initialization state for the outer loop is,</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">int</span> i <span style="color: #000080;">=</span> <span style="color: #0000dd;">0</span><span style="color: #008080;">;</span></div></div>
<p>The initialization state for the inner loop is,</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">char</span> j <span style="color: #000080;">=</span> <span style="color: #FF0000;">'A'</span><span style="color: #008080;">;</span></div></div>
<p>Notice that the whole inner loop has been placed just after the opening brace of the outer loop. Only one statement prints each character, and this statement has to be in the inner loop. This is because loops access one cell per combined iteration. The statement is:</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000dd;">cout</span> <span style="color: #000080;">&lt;&lt;</span> j <span style="color: #000080;">&lt;&lt;</span> <span style="color: #FF0000;">' '</span><span style="color: #008080;">;</span></div></div>
<p>The uppercase alphabet can be handled like whole numbers (positive integers). </p>
<p>The while condition for the inner loop is,</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">while</span> <span style="color: #008000;">&#40;</span>j <span style="color: #000080;">&lt;</span> <span style="color: #FF0000;">'E'</span><span style="color: #008000;">&#41;</span></div></div>
<p>at the end of the inner loop core, but not at the end of the inner loop code. This means the fifth letter ‘E’ and beyond must not be displayed. After the inner loop has displayed 4 elements in a row, the initial state is reset or re-initialized with the statement,</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; j <span style="color: #000080;">=</span> <span style="color: #FF0000;">'A'</span><span style="color: #008080;">;</span></div></div>
<p>which is not preceded by the reserved word, char. Preceding it with the reserved word, char will be declaring a new variable, which will not be part of the two-dimensional iteration. The statement after that, i.e.</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000dd;">cout</span> <span style="color: #000080;">&lt;&lt;</span> endl<span style="color: #008080;">;</span></div></div>
<p>forces the cursor of the screen to the next line so that the next row can be printed on the next line.</p>
<p>After this statement, the essential code for the outer loop begins. The while condition for the outer loop is,</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#40;</span>i <span style="color: #000080;">&lt;</span> <span style="color: #0000dd;">5</span><span style="color: #008000;">&#41;</span></div></div>
<p>meaning the rows are counted as 0, 1, 2, 3, 4, still five numbers. </p>
<p>The code for the inner loop, to cause the printing of the next character (column repetition) in the same row, is:</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; j<span style="color: #000040;">++</span><span style="color: #008080;">;</span></div></div>
<p>When the while condition for the inner loop results in false, no further character is printed. The code for the outer loop to cause the printing of the next row (line repetition) is:</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; i<span style="color: #000040;">++</span><span style="color: #008080;">;</span></div></div>
<p>When the while condition for the outer loop results in false, no further row is printed. </p>
<p>And so, the variable, i has been used for the outer loop, the rows; and the variable, j has been used for the inner loop, the line characters. </p>
<h2 id="2">Nesting while-Loops</h2>
<p>The following program does the same thing as the above program:</p>
<div class="codecolorer-container cpp blackboard" style="border:1px solid #9F9F9F;width:435px;height:100%;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; <span style="color: #339900;">#include &lt;iostream&gt;</span><br />
&nbsp; &nbsp; <span style="color: #0000ff;">using</span> <span style="color: #0000ff;">namespace</span> std<span style="color: #008080;">;</span><br />
<br />
&nbsp; &nbsp; <span style="color: #0000ff;">int</span> main<span style="color: #008000;">&#40;</span><span style="color: #008000;">&#41;</span><br />
&nbsp; &nbsp; <span style="color: #008000;">&#123;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">int</span> i <span style="color: #000080;">=</span> <span style="color: #0000dd;">0</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">char</span> j <span style="color: #000080;">=</span> <span style="color: #FF0000;">'A'</span><span style="color: #008080;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">while</span> <span style="color: #008000;">&#40;</span>i <span style="color: #000080;">&lt;</span> <span style="color: #0000dd;">5</span><span style="color: #008000;">&#41;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#123;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="color: #0000ff;">while</span> <span style="color: #008000;">&#40;</span>j <span style="color: #000080;">&lt;</span> <span style="color: #FF0000;">'E'</span><span style="color: #008000;">&#41;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#123;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000dd;">cout</span> <span style="color: #000080;">&lt;&lt;</span> j <span style="color: #000080;">&lt;&lt;</span> <span style="color: #FF0000;">' '</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; j<span style="color: #000040;">++</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#125;</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; j <span style="color: #000080;">=</span> <span style="color: #FF0000;">'A'</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000dd;">cout</span> <span style="color: #000080;">&lt;&lt;</span> endl<span style="color: #008080;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; i<span style="color: #000040;">++</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #008000;">&#125;</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; &nbsp; <br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #0000ff;">return</span> <span style="color: #0000dd;">0</span><span style="color: #008080;">;</span><br />
&nbsp; &nbsp; <span style="color: #008000;">&#125;</span></div></div>
<p>The output is:</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; A B C D <br />
&nbsp; &nbsp; A B C D <br />
&nbsp; &nbsp; A B C D <br />
&nbsp; &nbsp; A B C D <br />
&nbsp; &nbsp; A B C D</div></div>
<p>The algorithm of this code is almost the same as that of the previous one. The main difference is that, here, for each iteration for the inner or outer loop, the while condition is checked before the loop&#039;s body is executed. In the previous code, the body is executed before the while condition is checked.</p>
<h2 id="3">Nesting for-Loops</h2>
<p>The following program does the same thing as the above program:</p>
<div class="codecolorer-container bash blackboard" style="border:1px solid #9F9F9F;width:435px;"><div class="bash codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; <span style="color: #666666; font-style: italic;">#include &lt;iostream&gt;</span><br />
&nbsp; &nbsp; using namespace std;<br />
<br />
&nbsp; &nbsp; int main<span style="color: #7a0874; font-weight: bold;">&#40;</span><span style="color: #7a0874; font-weight: bold;">&#41;</span><br />
&nbsp; &nbsp; <span style="color: #7a0874; font-weight: bold;">&#123;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #000000; font-weight: bold;">for</span> <span style="color: #7a0874; font-weight: bold;">&#40;</span>int i = <span style="color: #000000;">0</span>; i <span style="color: #000000; font-weight: bold;">&lt;</span> <span style="color: #000000;">5</span>; i++<span style="color: #7a0874; font-weight: bold;">&#41;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #7a0874; font-weight: bold;">&#123;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="color: #000000; font-weight: bold;">for</span> <span style="color: #7a0874; font-weight: bold;">&#40;</span>char j = <span style="color: #ff0000;">'A'</span>; j <span style="color: #000000; font-weight: bold;">&lt;</span> <span style="color: #ff0000;">'E'</span>; j++<span style="color: #7a0874; font-weight: bold;">&#41;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #7a0874; font-weight: bold;">&#123;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; cout <span style="color: #000000; font-weight: bold;">&lt;&lt;</span> j <span style="color: #000000; font-weight: bold;">&lt;&lt;</span> <span style="color: #ff0000;">' '</span>;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #7a0874; font-weight: bold;">&#125;</span><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; cout <span style="color: #000000; font-weight: bold;">&lt;&lt;</span> endl;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #7a0874; font-weight: bold;">&#125;</span><br />
&nbsp; &nbsp; &nbsp; <br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #7a0874; font-weight: bold;">return</span> <span style="color: #000000;">0</span>;<br />
&nbsp; &nbsp; <span style="color: #7a0874; font-weight: bold;">&#125;</span></div></div>
<p>The output is:</p>
<div class="codecolorer-container cpp default" style="overflow:auto;white-space:nowrap;border:1px solid #9F9F9F;width:435px;"><div class="cpp codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">&nbsp; &nbsp; A B C D <br />
&nbsp; &nbsp; A B C D <br />
&nbsp; &nbsp; A B C D <br />
&nbsp; &nbsp; A B C D <br />
&nbsp; &nbsp; A B C D</div></div>
<p>There are certain things to note here: The nested for-loop is shorter in height than their other counterparts. The incrementing statement for the inner loop has been removed from its body and taken into the parentheses for its for-loop. The incrementing statement for the outer loop has been removed from its body and taken into the parentheses for its for-loop. The re-initialization (resetting) of the inner loop variable, j is no longer needed as a statement for the outer loop; because the initialization statement for the inner for-loop re-initializes itself for each iteration of the outer loop.</p>
<p>Do not forget that the for-loop is a classical and simplified form of the other two loops.</p>
<h2 id="4">Conclusion</h2>
<p>A loop has three important features: an initialization state, the while condition, and the push for the next repetition of the body of the loop. When one loop is nested into another, these three features must be considered for each of the corresponding loops. All the above-nested loops are one-level nesting loops. It is helpful to think of a one-level nesting, of loops, as a two-dimensional layout. The code of the nested loop is what accesses each cell for the 2D layout. This tutorial has given the reader the basics of loop nesting.</p>
</div>
