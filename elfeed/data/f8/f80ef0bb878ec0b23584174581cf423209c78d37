<div id="wpbody">Yield is a keyword/expression that is used to halt the implementation of the generator function. A generator function is similar to other functions but they are different in such a way that the value returned in the generator function is the yield keyword. Nested functions or callbacks cannot allow yield expressions.  Two characteristics are observed in objects returned by yield expressions, value, and done, which are the actual value and Boolean value respectively.  When the generator function is fully done, then the Boolean value is returned true and vice versa.</p>
<p>If the yield expression is being paused then it will pause the generator function too and it will only restart when the next method is being called until another return expression. The syntax of yield expression/keyword is as follows:</p>
<div class="codecolorer-container javascript blackboard" style="border:1px solid #9F9F9F;width:435px;"><div class="javascript codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;"><span style="color: #000066; font-weight: bold;">function</span><span style="color: #339933;">*</span> name<span style="color: #009900;">&#40;</span>arguments<span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#123;</span>statements<span style="color: #009900;">&#125;</span></div></div>
<p>Where <strong>name </strong>represents the name of the function, <strong>arguments </strong>are the parameters being passed for the function and <strong>statements </strong>represent the body of the function.</p>
<p>Following are the features of yield* expression/keywords:</p>
<ul>
<li>Memory efficient</li>
<li>Lazy evaluation</li>
<li>Control flows asynchronously</li>
</ul>
<p>Now we are going to illustrate an example through which you can easily understand how to use yield* keyword/expression in JavaScript.</p>
<div class="codecolorer-container javascript blackboard" style="border:1px solid #9F9F9F;width:435px;height:100%;"><div class="javascript codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;"><span style="color: #000066; font-weight: bold;">function</span><span style="color: #339933;">*</span> showNum<span style="color: #009900;">&#40;</span>x<span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#123;</span><br />
<br />
&nbsp; &nbsp; while <span style="color: #009900;">&#40;</span>x <span style="color: #339933;">&gt;</span> <span style="color: #CC0000;">0</span><span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#123;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; yield x<span style="color: #339933;">--;</span><br />
<br />
&nbsp; &nbsp; <span style="color: #009900;">&#125;</span><br />
<br />
<span style="color: #009900;">&#125;</span><br />
<br />
&nbsp;<br />
<span style="color: #006600; font-style: italic;">//instance is created for function showNum</span><br />
<br />
<span style="color: #000066; font-weight: bold;">const</span> generator_val <span style="color: #339933;">=</span> showNum<span style="color: #009900;">&#40;</span><span style="color: #CC0000;">4</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp;<br />
<span style="color: #006600; font-style: italic;">//return 4 as 4 is passed to the function showNum yield expression</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>generator_val.<span style="color: #660066;">next</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span>.<span style="color: #660066;">value</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
<span style="color: #006600; font-style: italic;">// return 3</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>generator_val.<span style="color: #660066;">next</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span>.<span style="color: #660066;">value</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
<span style="color: #006600; font-style: italic;">//return 2</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>generator_val.<span style="color: #660066;">next</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span>.<span style="color: #660066;">value</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
<span style="color: #006600; font-style: italic;">//return 1</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>generator_val.<span style="color: #660066;">next</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span>.<span style="color: #660066;">value</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span></div></div>
<p><strong>Output</strong></p>
<p><img src="https://linuxhint.com/wp-content/uploads/2021/11/1-18.jpg" alt="" width="467" height="211" class="aligncenter size-full wp-image-131585" srcset="https://linuxhint.com/wp-content/uploads/2021/11/1-18.jpg 467w, https://linuxhint.com/wp-content/uploads/2021/11/1-18-300x136.jpg 300w" sizes="(max-width: 467px) 100vw, 467px" /></p>
<p>On the other hand, the <strong>yield* is a keyword/expression</strong> that can be used to represent an iterative object or other generator function. The yield* iterates and returns value correspondingly until the Boolean value is true. The syntax of yield* expression/keyword is as follows:</p>
<div class="codecolorer-container javascript blackboard" style="border:1px solid #9F9F9F;width:435px;"><div class="javascript codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;">yield<span style="color: #339933;">*</span> expression</div></div>
<p>Now we are going to present an example of yield* expression/keyword.</p>
<div class="codecolorer-container javascript blackboard" style="border:1px solid #9F9F9F;width:435px;height:100%;"><div class="javascript codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;"><span style="color: #339933;">&lt;</span>html<span style="color: #339933;">&gt;</span><br />
<br />
<span style="color: #339933;">&lt;</span>head<span style="color: #339933;">&gt;</span><br />
<br />
&nbsp; &nbsp; <span style="color: #339933;">&lt;</span>title<span style="color: #339933;">&gt;</span>JavaScript yield<span style="color: #339933;">*</span> keyword<span style="color: #339933;">/</span>expression<span style="color: #339933;">&lt;/</span>title<span style="color: #339933;">&gt;</span><br />
<br />
<span style="color: #339933;">&lt;/</span>head<span style="color: #339933;">&gt;</span><br />
<br />
<span style="color: #339933;">&lt;</span>body<span style="color: #339933;">&gt;</span><br />
<br />
&nbsp; &nbsp; <span style="color: #339933;">&lt;</span>script<span style="color: #339933;">&gt;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #000066; font-weight: bold;">function</span><span style="color: #339933;">*</span> first_func<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#123;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; yield <span style="color: #CC0000;">10</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #009900;">&#125;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #000066; font-weight: bold;">function</span><span style="color: #339933;">*</span> second_func<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#123;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; yield<span style="color: #339933;">*</span> first_func<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #009900;">&#125;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #000066; font-weight: bold;">const</span> generator_iterator <span style="color: #339933;">=</span> second_func<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>generator_iterator.<span style="color: #660066;">next</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span>.<span style="color: #660066;">value</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp;<br />
&nbsp; &nbsp; <span style="color: #339933;">&lt;/</span>script<span style="color: #339933;">&gt;</span><br />
<br />
<span style="color: #339933;">&lt;/</span>body<span style="color: #339933;">&gt;</span><br />
<br />
<span style="color: #339933;">&lt;/</span>html<span style="color: #339933;">&gt;</span></div></div>
<p><strong>Output</strong></p>
<p><img src="https://linuxhint.com/wp-content/uploads/2021/11/2-18.jpg" alt="" width="523" height="284" class="aligncenter size-full wp-image-131586" srcset="https://linuxhint.com/wp-content/uploads/2021/11/2-18.jpg 523w, https://linuxhint.com/wp-content/uploads/2021/11/2-18-300x163.jpg 300w" sizes="(max-width: 523px) 100vw, 523px" /></p>
<p><strong>Example</strong></p>
<p>In this example, generatorfunc1() function returns yielded values through next() function similar to those values that are yielded through generatorfunc2() function. Subsequently, through this generatorfunc2() function, we can easily insert more generators as much as we can.</p>
<div class="codecolorer-container javascript blackboard" style="border:1px solid #9F9F9F;width:435px;height:100%;"><div class="javascript codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;"><span style="color: #339933;">&lt;</span>html<span style="color: #339933;">&gt;</span><br />
<br />
<span style="color: #339933;">&lt;</span>head<span style="color: #339933;">&gt;</span><br />
<br />
&nbsp; &nbsp; <span style="color: #339933;">&lt;</span>title<span style="color: #339933;">&gt;</span>JavaScript yield<span style="color: #339933;">*</span> representing other generator <span style="color: #339933;">&lt;/</span>title<span style="color: #339933;">&gt;</span><br />
<br />
<span style="color: #339933;">&lt;/</span>head<span style="color: #339933;">&gt;</span><br />
<br />
<span style="color: #339933;">&lt;</span>body<span style="color: #339933;">&gt;</span><br />
<br />
&nbsp; &nbsp; <span style="color: #339933;">&lt;</span>script<span style="color: #339933;">&gt;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #000066; font-weight: bold;">function</span><span style="color: #339933;">*</span> generatorfunc1<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#123;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; yield <span style="color: #CC0000;">22</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; yield <span style="color: #CC0000;">33</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; yield <span style="color: #CC0000;">44</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #009900;">&#125;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #000066; font-weight: bold;">function</span><span style="color: #339933;">*</span> generatorfunc2<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#123;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; yield <span style="color: #CC0000;">11</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; yield<span style="color: #339933;">*</span> generatorfunc1<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; yield <span style="color: #CC0000;">55</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #009900;">&#125;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #000066; font-weight: bold;">const</span> iterative_value <span style="color: #339933;">=</span> generatorfunc2<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #006600; font-style: italic;">// it return value 11 whereas done i.e. boolean value is false</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>iterative_value.<span style="color: #660066;">next</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #006600; font-style: italic;">// it return value 22 whereas done i.e. boolean value is false</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>iterative_value.<span style="color: #660066;">next</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #006600; font-style: italic;">// it return value 33 whereas done i.e. boolean value is false</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>iterative_value.<span style="color: #660066;">next</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #006600; font-style: italic;">// it return value 44 whereas done i.e. boolean value is false</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>iterative_value.<span style="color: #660066;">next</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #006600; font-style: italic;">// it return value 55 whereas done i.e. boolean value is false</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>iterative_value.<span style="color: #660066;">next</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="color: #006600; font-style: italic;">// it return undefined value whereas done i.e. boolean value is true</span><br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>iterative_value.<span style="color: #660066;">next</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
&nbsp; &nbsp; <span style="color: #339933;">&lt;/</span>script<span style="color: #339933;">&gt;</span><br />
<br />
<span style="color: #339933;">&lt;/</span>body<span style="color: #339933;">&gt;</span><br />
<br />
<span style="color: #339933;">&lt;/</span>html<span style="color: #339933;">&gt;</span></div></div>
<p><strong>Output</strong></p>
<p><img src="https://linuxhint.com/wp-content/uploads/2021/11/3-16.jpg" alt="" width="624" height="248" class="aligncenter size-full wp-image-131587" srcset="https://linuxhint.com/wp-content/uploads/2021/11/3-16.jpg 624w, https://linuxhint.com/wp-content/uploads/2021/11/3-16-300x119.jpg 300w" sizes="(max-width: 624px) 100vw, 624px" /></p>
<h2>Conclusion</h2>
<p>After reading this article, you are familiar with the yield* keyword/expression. If you are using the yield* expression then you cannot face the callback issues. The concept behind yield* expression is that function can voluntarily resume or stop till it acquires what it needs. We also enlisted examples that help you to understand the better usage of yield* expression/keyword in JavaScript.
 </p></div>
