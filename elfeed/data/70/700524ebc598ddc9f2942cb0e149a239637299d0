<p>Today I’ll follow-up on <a href="/blog/2021/10/27/submitting-the-current-input-in-selectrum/">yesterday’s Selectrum article</a> with
a bit more details on how to submit the current input in <code class="language-plaintext highlighter-rouge">ido</code> (as opposed to the currently selected candidate).</p>

<p>There are 3 ways in which you can do this, depending on the underlying <code class="language-plaintext highlighter-rouge">ido</code>
command:</p>

<ul>
  <li>if you’re using <code class="language-plaintext highlighter-rouge">ido-find-file</code> (e.g. you’ve pressed <code class="language-plaintext highlighter-rouge">C-x C-f</code>), then you can
switch back to the regular <code class="language-plaintext highlighter-rouge">find-file</code> using <code class="language-plaintext highlighter-rouge">C-f</code>.</li>
  <li>similarly if you’re using <code class="language-plaintext highlighter-rouge">ido-switch-buffer</code> (or something else to do with buffers), they you can switch back to the
plain old <code class="language-plaintext highlighter-rouge">switch-buffer</code> using <code class="language-plaintext highlighter-rouge">C-b</code>.</li>
  <li>regardless of the current <code class="language-plaintext highlighter-rouge">ido</code> command you can always use <code class="language-plaintext highlighter-rouge">C-j</code> (same as in Selectrum).</li>
</ul>

<p>Here’s a small example:</p>

<p><img src="/assets/images/ido_current_input_file.png" alt="ido_current_input_file.png" /></p>

<p>If I want to create a file named “blast” I can either type <code class="language-plaintext highlighter-rouge">C-f</code> to go back to the regular <code class="language-plaintext highlighter-rouge">find-file</code> command,
or <code class="language-plaintext highlighter-rouge">C-j</code> to immediately submit the text that I’ve already written. I guess that in almost all situations
using <code class="language-plaintext highlighter-rouge">C-j</code> is going to be the optimal course of action.</p>

<p>By the way, turns out that the built-in <code class="language-plaintext highlighter-rouge">icomplete</code> mode, also uses <code class="language-plaintext highlighter-rouge">C-j</code> for submitting the current input.</p>