<p>
Hunter, like most other schools has gone remote. I taught my first two
<a href="https://cestlaz.github.io/post/teaching-online-day-01/">online classes</a> on Thursday. Currently, I&#39;m using Zoom for synchronous
stuff and a mailing list and slack for async. </p>
<p>
There are still some missing pieces. When we&#39;re all together, it&#39;s
easy to look at a student&#39;s work and talk them through issues. It&#39;s
also easy to get students to work together, at least to a point. With
everyone locked up in their own homes, real time collaboration is
harder. Sure, we can use tools like GitHub issues and pull requests
for async code commentary but what about live help and pairing? If
we want to use an online environment, <a href="https://repl.it">repl.it</a>  - a platform I very
much like fits the bill. On the other hand, what do you use if you
want to work collaboratively in real time using a local editor. </p>
<p>
That&#39;s where <a href="https://floobits.com">Floobits</a> comes in. Floobits is a platform that provides
for collaborative live editing. It works with Emacs, Neovim, Sublime
Text, Atom, and IntelliJ (and maybe all the Jetbrains IDEs). They
support a free tier which only has public workspaces but that&#39;s fine,
as far as I&#39;m concerned, for educational purposes. </p>
<p>
Check out the video to see how I set it up:</p>
<iframe width="560" height="315"
src="https://www.youtube.com/embed/-0x4PV1EB5w" frameborder="0"
allow="accelerometer; autoplay; encrypted-media; gyroscope;
picture-in-picture" allowfullscreen></iframe>
