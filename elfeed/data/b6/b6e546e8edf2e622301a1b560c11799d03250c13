<div id="wpbody"> JavaScript is a programming language that gives web applications and web pages the ability to think and act by making them dynamic and interactive. When you are programming, it would often confuse you with which data structure to use for storing data that can easily be managed and understood. To hold collections of data, arrays and objects are commonly used data structures. Key-value pairs are stored in objects, while indexed lists are stored in arrays. Then came the ECMAScript 2015 which introduced another iterable object known as maps to provide developers more flexibility. So, in this post, we will discuss what a map object in JavaScript is and discuss some practical examples along with screenshots of the output.</p>
<h2>What is a map object?</h2>
<p>Map object like an ordinary object is a collection of elements that stores key-value pairs; however, the main difference is that the keys can be of any type. It should also be noted that the map object remembers the order of the key-value pair that was inserted in the map object. </p>
<p><strong>Syntax:</strong></p>
<div class="codecolorer-container javascript blackboard" style="border:1px solid #9F9F9F;width:435px;"><div class="javascript codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;"><span style="color: #000066; font-weight: bold;">var</span> map <span style="color: #339933;">=</span> <span style="color: #000066; font-weight: bold;">new</span> Map<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#91;</span>it<span style="color: #009900;">&#93;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span></div></div>
<p>Where <strong>it</strong> is optional and an iterable object whose elements are stored as key-value pairs.</p>
<p>Map object has some properties and methods which are defined below:</p>
<ul>
<li>For creation of new map -&gt; new Map()</li>
<li>To store a value using a key -&gt; map.set(key, value)</li>
<li>To get the value of the key -&gt; map.get(key)</li>
<li>If the key doesn’t exist then map.get(key) will return undefined</li>
<li>To remove everything from the map -&gt; map.clear()</li>
<li>To get the size of the map -&gt; map.size</li>
<li>To delete a value using the key -&gt; map.delete(key)</li>
</ul>
<h2>Map Object Example1</h2>
<p>In the code given below, first, we initiated the map object and then set the values. The first key that we set is a string key, the second is a numeric key and the third is a boolean key. Then we console log the result of getting the values of the keys provided. We also check the size of the map object which returns 3.</p>
<div class="codecolorer-container javascript blackboard" style="border:1px solid #9F9F9F;width:435px;"><div class="javascript codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;"><span style="color: #006600; font-style: italic;">// map creation</span><br />
<span style="color: #000066; font-weight: bold;">var</span> map <span style="color: #339933;">=</span> <span style="color: #000066; font-weight: bold;">new</span> Map<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<span style="color: #006600; font-style: italic;">// setting key value pairs to map</span><br />
map.<span style="color: #000066; font-weight: bold;">set</span><span style="color: #009900;">&#40;</span><span style="color: #3366CC;">'1'</span><span style="color: #339933;">,</span> <span style="color: #3366CC;">'string'</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> &nbsp; <span style="color: #006600; font-style: italic;">// a string key</span><br />
map.<span style="color: #000066; font-weight: bold;">set</span><span style="color: #009900;">&#40;</span><span style="color: #CC0000;">1</span><span style="color: #339933;">,</span> <span style="color: #3366CC;">'number'</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> &nbsp; &nbsp; <span style="color: #006600; font-style: italic;">// a numeric key</span><br />
map.<span style="color: #000066; font-weight: bold;">set</span><span style="color: #009900;">&#40;</span><span style="color: #003366; font-weight: bold;">true</span><span style="color: #339933;">,</span> <span style="color: #3366CC;">'boolean'</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> <span style="color: #006600; font-style: italic;">// a boolean key</span><br />
<br />
<span style="color: #006600; font-style: italic;">//get info from map</span><br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span> map.<span style="color: #000066; font-weight: bold;">get</span><span style="color: #009900;">&#40;</span><span style="color: #CC0000;">1</span><span style="color: #009900;">&#41;</span> &nbsp; <span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> <span style="color: #006600; font-style: italic;">// number</span><br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span> map.<span style="color: #000066; font-weight: bold;">get</span><span style="color: #009900;">&#40;</span><span style="color: #3366CC;">'1'</span><span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> <span style="color: #006600; font-style: italic;">// string</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>map.<span style="color: #660066;">size</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> <span style="color: #006600; font-style: italic;">// 3</span></div></div>
<p><img src="https://linuxhint.com/wp-content/uploads/2021/11/What-is-a-Map-Object-in-JavaScript-1.png" alt="" width="136" height="132" class="aligncenter size-full wp-image-131670" /></p>
<h2>Map Object Example2</h2>
<p>We can also set the keys of the map object as objects. Let us demonstrate this with the help of the below-given code:</p>
<div class="codecolorer-container javascript blackboard" style="border:1px solid #9F9F9F;width:435px;"><div class="javascript codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;"><span style="color: #000066; font-weight: bold;">var</span> student1 <span style="color: #339933;">=</span> <span style="color: #009900;">&#123;</span> name<span style="color: #339933;">:</span> <span style="color: #3366CC;">&quot;Jhon&quot;</span> <span style="color: #009900;">&#125;</span><span style="color: #339933;">;</span><br />
<br />
<span style="color: #006600; font-style: italic;">// for every student, let's store their marks</span><br />
<span style="color: #000066; font-weight: bold;">var</span> marks <span style="color: #339933;">=</span> <span style="color: #000066; font-weight: bold;">new</span> Map<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
<span style="color: #006600; font-style: italic;">// student1 is the key for the map and student 1 itself is an object</span><br />
marks.<span style="color: #000066; font-weight: bold;">set</span><span style="color: #009900;">&#40;</span>student1<span style="color: #339933;">,</span> <span style="color: #CC0000;">93</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span> marks.<span style="color: #000066; font-weight: bold;">get</span><span style="color: #009900;">&#40;</span>student1<span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span> <span style="color: #006600; font-style: italic;">// 93</span></div></div>
<p>In the above example, we first initiated an object with the name of <strong>student1</strong> and then created a map object. After that, we set the marks of student1 and then console log the marks of student1. It should be noted that student1 itself is an object but acts as the key here. We get the output 93 as shown below:</p>
<p><img src="https://linuxhint.com/wp-content/uploads/2021/11/What-is-a-Map-Object-in-JavaScript-2.png" alt="" width="122" height="64" class="aligncenter size-full wp-image-131671" /></p>
<h2>Iterating over map keys</h2>
<p>Let us now see how to iterate over map keys for which purpose we will use a for loop and map.keys() method. The map.keys() method will return all the keys of the map object in the order they were inserted in the map object. </p>
<p>Let us initiate a map object and give names as keys and values as the job position. Then we implemented a for loop that will console log all the keys/names in the map object.</p>
<div class="codecolorer-container javascript blackboard" style="border:1px solid #9F9F9F;width:435px;"><div class="javascript codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;"><span style="color: #006600; font-style: italic;">// creating map object</span><br />
<span style="color: #000066; font-weight: bold;">var</span> employees <span style="color: #339933;">=</span> <span style="color: #000066; font-weight: bold;">new</span> Map<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#91;</span><br />
&nbsp; &nbsp; <span style="color: #009900;">&#91;</span><span style="color: #3366CC;">&quot;john&quot;</span><span style="color: #339933;">,</span> <span style="color: #3366CC;">'admin'</span><span style="color: #009900;">&#93;</span><span style="color: #339933;">,</span><br />
&nbsp; &nbsp; <span style="color: #009900;">&#91;</span><span style="color: #3366CC;">&quot;Sarah&quot;</span><span style="color: #339933;">,</span> <span style="color: #3366CC;">'editor'</span><span style="color: #009900;">&#93;</span><span style="color: #339933;">,</span><br />
&nbsp; &nbsp; <span style="color: #009900;">&#91;</span><span style="color: #3366CC;">&quot;Sam&quot;</span><span style="color: #339933;">,</span> <span style="color: #3366CC;">'writer'</span><span style="color: #009900;">&#93;</span><br />
<span style="color: #009900;">&#93;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
<span style="color: #006600; font-style: italic;">// for loop</span><br />
<span style="color: #000066; font-weight: bold;">for</span> <span style="color: #009900;">&#40;</span><span style="color: #000066; font-weight: bold;">var</span> name of employees.<span style="color: #660066;">keys</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#123;</span><br />
&nbsp; &nbsp; console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>name<span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<span style="color: #009900;">&#125;</span></div></div>
<p>The output of the above code is:</p>
<p><img src="https://linuxhint.com/wp-content/uploads/2021/11/What-is-a-Map-Object-in-JavaScript-3.png" alt="" width="116" height="129" class="aligncenter size-full wp-image-131672" /></p>
<p>To iterate over map values, we will simply change <strong>employees.keys()</strong> to <strong>employees.values():</strong></p>
<div class="codecolorer-container javascript blackboard" style="border:1px solid #9F9F9F;width:435px;"><div class="javascript codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;"><span style="color: #006600; font-style: italic;">// creating map object</span><br />
<span style="color: #000066; font-weight: bold;">var</span> employees <span style="color: #339933;">=</span> <span style="color: #000066; font-weight: bold;">new</span> Map<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#91;</span><br />
&nbsp; &nbsp; <span style="color: #009900;">&#91;</span><span style="color: #3366CC;">&quot;john&quot;</span><span style="color: #339933;">,</span> <span style="color: #3366CC;">'admin'</span><span style="color: #009900;">&#93;</span><span style="color: #339933;">,</span><br />
&nbsp; &nbsp; <span style="color: #009900;">&#91;</span><span style="color: #3366CC;">&quot;Sarah&quot;</span><span style="color: #339933;">,</span> <span style="color: #3366CC;">'editor'</span><span style="color: #009900;">&#93;</span><span style="color: #339933;">,</span><br />
&nbsp; &nbsp; <span style="color: #009900;">&#91;</span><span style="color: #3366CC;">&quot;Sam&quot;</span><span style="color: #339933;">,</span> <span style="color: #3366CC;">'writer'</span><span style="color: #009900;">&#93;</span><br />
<span style="color: #009900;">&#93;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
<span style="color: #006600; font-style: italic;">// for loop</span><br />
<span style="color: #000066; font-weight: bold;">for</span> <span style="color: #009900;">&#40;</span><span style="color: #000066; font-weight: bold;">var</span> position of employees.<span style="color: #660066;">values</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#123;</span><br />
&nbsp; &nbsp; console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>position<span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<span style="color: #009900;">&#125;</span></div></div>
<p><img src="https://linuxhint.com/wp-content/uploads/2021/11/What-is-a-Map-Object-in-JavaScript-4.png" alt="" width="130" height="128" class="aligncenter size-full wp-image-131673" /></p>
<h2>Deleting Element using Key</h2>
<p>In this example we will delete an entry in the map object using the <strong>delete()</strong> method:</p>
<div class="codecolorer-container javascript blackboard" style="border:1px solid #9F9F9F;width:435px;"><div class="javascript codecolorer" style="padding:5px;font:normal 12px/1.4em Monaco, Lucida Console, monospace;white-space:nowrap;"><span style="color: #006600; font-style: italic;">// creating map object</span><br />
<span style="color: #000066; font-weight: bold;">var</span> employees <span style="color: #339933;">=</span> <span style="color: #000066; font-weight: bold;">new</span> Map<span style="color: #009900;">&#40;</span><span style="color: #009900;">&#91;</span><br />
&nbsp; &nbsp; <span style="color: #009900;">&#91;</span><span style="color: #3366CC;">&quot;john&quot;</span><span style="color: #339933;">,</span> <span style="color: #3366CC;">'admin'</span><span style="color: #009900;">&#93;</span><span style="color: #339933;">,</span><br />
&nbsp; &nbsp; <span style="color: #009900;">&#91;</span><span style="color: #3366CC;">&quot;Sarah&quot;</span><span style="color: #339933;">,</span> <span style="color: #3366CC;">'editor'</span><span style="color: #009900;">&#93;</span><span style="color: #339933;">,</span><br />
&nbsp; &nbsp; <span style="color: #009900;">&#91;</span><span style="color: #3366CC;">&quot;Sam&quot;</span><span style="color: #339933;">,</span> <span style="color: #3366CC;">'writer'</span><span style="color: #009900;">&#93;</span><br />
<span style="color: #009900;">&#93;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
<span style="color: #006600; font-style: italic;">// deleting john from employees</span><br />
<br />
employees.<span style="color: #000066; font-weight: bold;">delete</span><span style="color: #009900;">&#40;</span><span style="color: #3366CC;">&quot;john&quot;</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<br />
<span style="color: #006600; font-style: italic;">// for loop</span><br />
<span style="color: #000066; font-weight: bold;">for</span> <span style="color: #009900;">&#40;</span><span style="color: #000066; font-weight: bold;">var</span> name of employees.<span style="color: #660066;">keys</span><span style="color: #009900;">&#40;</span><span style="color: #009900;">&#41;</span><span style="color: #009900;">&#41;</span> <span style="color: #009900;">&#123;</span><br />
&nbsp; &nbsp; console.<span style="color: #660066;">log</span><span style="color: #009900;">&#40;</span>name<span style="color: #009900;">&#41;</span><span style="color: #339933;">;</span><br />
<span style="color: #009900;">&#125;</span></div></div>
<p>We can see that <strong>John </strong>has been deleted and missing from the output:</p>
<p><img src="https://linuxhint.com/wp-content/uploads/2021/11/What-is-a-Map-Object-in-JavaScript-5.png" alt="" width="106" height="88" class="aligncenter size-full wp-image-131674" /></p>
<h2>Conclusion</h2>
<p>Map object was introduced in ES6 and prior to map object, ordinary objects were used. However, an object has some deficiencies like an object always has a default key prototype and one cannot use an object as a key. To solve this problem, the map was introduced which is a collection of elements stored in key-value pairs just like objects but here keys can be of any type. In this post, we saw a map object in JavaScript and discussed two examples. Apart from that, we also implemented and looked at different methods and properties of map objects of JavaScript.
</p></div>
