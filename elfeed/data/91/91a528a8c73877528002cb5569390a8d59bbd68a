<p>
I primarily program in four languages these days -</p>
<ul>
<li>
<p>Clojure </p>
</li>
<li>
<p>C++</p>
</li>
<li>
<p>Python</p>
</li>
<li>
<p>Java</p>
</li>
</ul>
<p>And most of the time, my Emacs configuration has handled each one
differently. Cider for Clojure, Irony for C++, Elpy and Jedi for
Python and Java I could never figure out. This is of course on top of
tools that work across languages like company for completions, or
flycheck for general language syntax checking. </p>
<p>
A while ago I heard about lsp-mode - Language Server Protocol
mode. Basically, you set up the mode and it connects to back end
language servers. The idea is to keep configuration down and provide a
consistent interface across langauges while leveraging the types of
support these back ends can provide. I wrote a bit about lsp-mode and
made a video and you can find it <a href="https://cestlaz.github.io/post/using-emacs-58-lsp-mode/">here</a>. </p>
<p>
When I first tried LSP it just didn&#39;t work. I tried it again later and
it basically worked but was finicky. Configuration took more work than
I wanted, particularly for customizing per language and even when
working it didn&#39;t work as expected. </p>
<p>
This past summer, I had to teach using Java so I tried lsp-mode
again. Setup wasn&#39;t too bad and most of the issues were more related
to Java than to lsp-mode but I didn&#39;t like the results. While it might
be great for the professional developer there were too many popups and
made the screen way too busy for teaching. </p>
<p>
Still, the idea of a single simple configuration was enticing.</p>
<p>
So, when I decided to get my configuration together for the Fall
semester I decided to try yet again and discovered <a href="https://github.com/joaotavora/eglot">eglot</a> for
Emacs. Eglot turned out to be easier to install and gave me more of
the experience I was looking for.</p>
<p>
To start, I set it up for C++ using</p>
<div class="src src-text">
<div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-text" data-lang="text">(use-package eglot :ensure t)
(add-to-list &#39;eglot-server-programs &#39;((c++-mode c-mode) &#34;clangd-10&#34;))
(add-hook &#39;c-mode-hook &#39;eglot-ensure)
(add-hook &#39;c++-mode-hook &#39;eglot-ensure)</code></pre></div>
</div>
<p>
Eglot defaults to <a href="https://github.com/MaskRay/ccls">ccls</a> as a C++ language server. I didn&#39;t want to have
to build it but was able to <code>apt-get install clangd-10</code> and use that
instead by adding clangd-10 to the eglot-server-programs in the second
configuration line.</p>
<p>
For python I had to install <a href="https://github.com/palantir/python-language-server">pyls</a>, the Python Language Server but that
was easy to do and then I just had to add <code>(add-hook &#39;python-mode-hook
&#39;eglot-ensure)</code> to my config.</p>
<p>
Finally, Java was more of an issue I had to get <a href="https://github.com/eclipse/eclipse.jdt.ls">eclipse.jdt.ls</a> on my
system. It turns out that lsp-mode installed it for me already so I
just had to point to it:</p>
<div class="src src-text">
<div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-text" data-lang="text">(defconst my-eclipse-jdt-home &#34;/home/zamansky/.emacs.d/.cache/lsp/eclipse.jdt.ls/plugins/org.eclipse.equinox.launcher_1.5.800.v20200727-1323.jar&#34;)
(defun my-eglot-eclipse-jdt-contact (interactive)
  &#34;Contact with the jdt server input INTERACTIVE.&#34;
  (let ((cp (getenv &#34;CLASSPATH&#34;)))
    (setenv &#34;CLASSPATH&#34; (concat cp &#34;:&#34; my-eclipse-jdt-home))
    (unwind-protect (eglot--eclipse-jdt-contact nil)
      (setenv &#34;CLASSPATH&#34; cp))))
(setcdr (assq &#39;java-mode eglot-server-programs) #&#39;my-eglot-eclipse-jdt-contact)
(add-hook &#39;java-mode-hook &#39;eglot-ensure)</code></pre></div>
</div>
<p>
So far, I&#39;m liking eglot very much. I&#39;ll probably check lsp-mode out
again somewhere down the line but as of now it&#39;s Cider for Clojure and
Eglot for everything else.</p>
<p>
The video has a run through and demo. Check it out.</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/ROnceqt3kpE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
</iframe>
