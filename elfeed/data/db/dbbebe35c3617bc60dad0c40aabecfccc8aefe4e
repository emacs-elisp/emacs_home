<p>
I made this video write after I made the <a href="https://cestlaz.github.io/post/using-emacs-71-openwith/">openwith</a> one so even though
I don&#39;t mention anything in this video, I wanted to share some updates
on dired and openwith.</p>
<p>
I got a lot of suggestions on alternate ways to achieve the workflow I
was seeking - being able to open a file using an external viewer. One
person noted that under newer versions of Emacs, the <code>W</code> key is bound
to the command <code>(browse-url-of-dired-file)</code> which does exactly what I
wanted. Problem solved with no customizing or extra packages. </p>
<p>
It&#39;s a good thing I got a solution when I did - I needed it. I&#39;ve been
reviewing applications for my new Teacher Certification program. It&#39;s
a 21 credit graduate program that results in participants receiving a
brand spanking new New York State Computer Science teaching
license. The applications consist of a bunch of files incuding PDFs,
images (photos), doc and text files and source code samples. I&#39;ve got
all the applications in a Google drive which I&#39;ve downloaded and now I
can just drop into dired and go through each directory, hit <code>enter</code> for
source code and it&#39;s loaded in an Emacs buffer and <code>W</code> for everything
else.</p>
<p>
Now for today&#39;s video. It&#39;s about a small customization I made to the
elfeed RSS reader. It&#39;s also about the advantages and limitations of
Emacs built in documentation. I&#39;m not particularly strong in elisp -
I&#39;m a dabbler at best but even so, using Emac&#39;s built in help I was
able to get to elisp code that was very close to what I wanted to do
and then customize it. It also looks at the limitations - there still
some code that I looked at that I have no clue as to what it does.</p>
<p>
Specifically, elfeed lets you open a link in an external brower by
hitting <code>b</code>. That&#39;s how I like reading most of my feeds. I&#39;ll scroll
up and down and hit <code>b</code> for what I want to read. The trouble is that
when you hit <code>b</code> focus goes to the browser and at the same time,
elfeed marks the entry as read and advances. I want it not to mark as
read. After I made the video, I decided I also didn&#39;t want elfeed to
advance to the next story. </p>
<p>
Here&#39;s what I did.</p>
<p>
I went in to elfeed and instead of typing <code>b</code> on an entry, I ran
<code>(describe-key</code>)~ which is bound to <code>C-h k</code>. I then typed <code>b</code> which
revealed that it ran <code>elfeed-search-browse-url</code>. I followed the
hyperlinks in the help to open <code>elfeed-search.el</code> and found the
function. From there I just copied it over to my Emacs config, tweaked
it and voila, problem solved.</p>
<p>
The video has all the details. Enjoy.</p>
<iframe width="560" height="315"
src="https://www.youtube.com/embed/G1NGNR40lB4" frameborder="0"
allow="accelerometer; autoplay; encrypted-media; gyroscope;
picture-in-picture" allowfullscreen></iframe>
