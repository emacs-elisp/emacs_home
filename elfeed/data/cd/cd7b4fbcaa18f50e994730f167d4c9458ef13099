<div id="content">
 <p>
If you like what I’m doing, consider chipping in, every
little bit helps to keep me going :)
</p>

 <center> <script src="https://liberapay.com/Ambrevar/widgets/button.js"></script></center>
 <center> <noscript> <a href="https://liberapay.com/Ambrevar/donate"> <img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></img></a></noscript></center>

 <p>
Thank you!
</p>

 <ul class="org-ul"> <li>Common Lisp
 <ul class="org-ul"> <li> <a href="https://github.com/40ants/cl-prevalence">cl-prevalence</a> (contributor)</li>
 <li> <a href="https://github.com/hu-dwim/hu.dwim.defclass-star">defclass-star</a> (contributor)</li>
 <li> <a href="https://gitlab.com/ambrevar/fof">FOF</a></li>
 <li> <a href="https://gitlab.com/ambrevar/lisp-repl-core-dumper">Lisp REPL core dumper</a></li>
 <li> <a href="https://github.com/fukamachi/quri">Quri</a> (contributor)</li>
 <li> <a href="https://github.com/ruricolist/serapeum">Serapeum</a> (contributor)</li>
</ul></li>
 <li> <a href="../demlo">Demlo</a> - A dynamic and extensible music library organizer</li>
 <li> <a href="https://gitlab.com/ambrevar/dotfiles">dotfiles</a> - Emacs (EXWM, Evil, Helm, mu4e, notmuch, Eshell…), Guix, custom xkb layout</li>
 <li>Emacs
 <ul class="org-ul"> <li> <a href="https://gitlab.com/ambrevar/emacs-disk-usage">Emacs Disk Usage</a></li>
 <li> <a href="https://savannah.gnu.org/project/memberlist.php?group=emms">EMMS</a> ( <a href="https://savannah.gnu.org/project/memberlist.php?group=emms">contributor</a>)</li>
 <li> <a href="../evil-collection/index.html">Evil collection</a></li>
 <li> <a href="https://gitlab.com/Ambrevar/emacs-fish-completion">Emacs fish completion</a></li>
 <li> <a href="https://gitlab.com/Ambrevar/emacs-gif-screencast">Emacs GIF Screencast</a></li>
 <li> <a href="https://gitlab.com/Ambrevar/emacs-webfeeder">Emacs Webfeeder</a></li>
 <li> <a href="https://github.com/emacs-helm/helm">Helm</a> ( <a href="https://github.com/orgs/emacs-helm/people">contributor</a>)</li>
 <li> <a href="https://github.com/emacs-helm/helm-eww">Helm-EWW</a></li>
 <li> <a href="https://github.com/emacs-helm/helm-exwm">Helm-EXWM</a></li>
 <li> <a href="https://github.com/emacs-helm/helm-fish-completion">Helm Fish Completion</a></li>
 <li> <a href="https://github.com/emacs-helm/helm-notmuch">Helm-notmuch</a> (contributor)</li>
 <li> <a href="https://github.com/emacs-helm/helm-pass">Helm-pass</a> (contributor)</li>
 <li> <a href="https://github.com/emacs-helm/helm-selector">Helm Selector</a></li>
 <li> <a href="https://github.com/emacs-helm/helm-slime">Helm SLIME</a> (contributor)</li>
 <li> <a href="https://github.com/emacs-helm/helm-sly">Helm SLY</a></li>
 <li> <a href="https://github.com/emacs-helm/helm-switch-to-repl">Helm Switch to REPL</a></li>
 <li> <a href="../helm-system-packages/index.html">Helm System Packages</a></li>
 <li> <a href="../mu4e-conversation/index.html">mu4e-conversation</a> - Show a complete thread in a single buffer</li>
 <li> <a href="https://gitlab.com/ambrevar/emacs-windower">Emacs windower</a></li>
</ul></li>
 <li> <a href="https://www.gnu.org/software/guix">Guix</a> ( <a href="https://savannah.gnu.org/project/memberlist.php?group=guix">contributor</a>)
 <ul class="org-ul"> <li> <a href="https://gitlab.com/nonguix/nonguix">Nonguix</a> (contributor)</li>
 <li> <a href="https://gitlab.com/guix-gaming-channels">Guix Gaming Channels</a></li>
</ul></li>
 <li> <a href="../hsync">hsync</a> - A filesystem hierarchy synchronizer</li>
 <li> <a href="https://nyxt.atlas.engineer">Nyxt</a> - The infinitely extensible web browser</li>
 <li> <a href="https://gitlab.com/ambrevar/project-euler">Project Euler solutions</a> - computational problems from  <a href="http://projecteuler.net/">Project Euler</a></li>
 <li> <a href="https://appdb.winehq.org/">Wine AppDB</a> - maintenance of a few games</li>
</ul> <div id="outline-container-org172b07c" class="outline-2">
 <h2 id="org172b07c">Past projects</h2>
 <div class="outline-text-2" id="text-org172b07c">
 <ul class="org-ul"> <li> <a href="https://www.archlinux.org/">Arch Linux</a> - Former  <a href="https://www.archlinux.org/people/trusted-users/#ambrevar">Trusted User</a>
 <ul class="org-ul"> <li> <a href="https://aur.archlinux.org/packages/?K=Ambrevar&SeB=m">AUR packages</a> (versioned  <a href="https://gitlab.com/ambrevar/archlinux-pkgbuilds">here</a>)</li>
 <li> <a href="https://www.archlinux.org/pacman">pacman patches</a></li>
</ul></li>
 <li> <a href="https://github.com/russross/blackfriday">Blackfriday</a> - a Markdown processor implemented in Go (contribution to v2 rewrite)
 <ul class="org-ul"> <li> <a href="https://gitlab.com/ambrevar/blackfriday-latex/">Blackfriday-LaTeX</a> - A LaTeX renderer for Blackfriday (v2 and above)</li>
</ul></li>
 <li> <a href="../inprogen">Inprogen</a> - An inverse procedural generator</li>
 <li> <a href="https://en.wikibooks.org/wiki/latex">LaTeX Wikibook</a> -  <a href="https://en.wikibooks.org/wiki/User:Ambrevar/LaTeX">contributions</a></li>
 <li> <a href="https://github.com/stevedonovan/luar">luar</a> - Lua reflection bindings for Go (collaborator)</li>
 <li> <a href="../perspector">Perspector</a> - A control-point-based perspective rectification tool</li>
 <li> <a href="https://gitlab.com/ambrevar/procedural-texture-generator">Procedural texture generator</a> - a tech demo</li>
</ul></div>
</div>
</div>