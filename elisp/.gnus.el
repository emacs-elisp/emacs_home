
(setq user-mail-address "cayetano.santos@inventati.org")
(setq user-full-name "Cayetano Santos")

(setq gnus-select-method
      '(nnimap "Mail"
               (nnimap-address "localhost")
               (nnimap-stream network)
               (nnimap-authenticator login)))

;; (setq gnus-select-method '(nntp "gnu.emacs"))
;; (add-to-list 'gnus-secondary-select-methods '(nntp "gnu.emacs.announce"))
